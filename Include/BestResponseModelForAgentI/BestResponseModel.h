/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */


#include "../Interfaces/DecPomdpInterface.h"
#include "../Interfaces/PomdpInterface.h"
#include "../Interfaces/FSCBase.h"
#include <map>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cfloat>
using namespace std;

class BestResponseModel :public PomdpInterface
{
private:
    DecPomdpInterface* DecPomdpModel;
    vector<FSCBase> FSCs;
    int CurrentOptimizingAgentI;
    int AgentNb;
    vector<string> States;
    vector<string> Actions;
    vector<string> Observations;
    vector<double> b0;
    vector<vector<int> > _m_DecPomdpStateFscNodeObsIndicies;// vector (eI -> {sI,NI,OI})
    vector<vector<int> > _m_IndiciesOfFSCsNodes;  // NI -> <n1,n2 ... n> 
    vector<vector<int> > _m_IndiciesOfObs; // OI -> <o1, o2 ... on>
    vector<double> _m_res_SumPrObsAgentI; // <N_newI, OI, NI> -> pb what if using unordered_map<string, int> / map<vector, int>?
    double ComputeSumPrObsAgentI(DecPomdpInterface* decpomdp, int optimizing_agentI, int OI, int s_newI, int JAI);
    void RecursiveBuildAllFSCsNodesIndicies(int depth, int& NI, vector<int> NodesIndicies);
    void RecursiveBuildAllObsIndicies(int depth, int& OI, vector<int> ObsIndicies);
    vector<int> NItoActionsIndicies(int NI);
    double ProbAllNodesTrans(int N_newI, int OI, int NI);
    int NI_size;
    int SizeOfS;
    int SizeOfA;
    int SizeOfObs;
    int SizeJAI;
    int SizeOI; // all other agents observation index size 
    int SizeDecPomdpStateSpace;
    int StateSizeBeforeElimination;
public:
    BestResponseModel(DecPomdpInterface* DecPomdp, vector<FSCBase>& FSCs,int optimizing_agentI);
    ~BestResponseModel(){};
    int GetCurrentOptmizingAgentIndex();
    double GetDiscount();
    int GetSizeOfS();
    int GetSizeOfA();
    int GetSizeOfObs();
    int GetSizeOfStateSizeBeforeElimination();
    std::vector<double> GetInitBelief();
    double TransFunc(int eI, int aI_opt_agentI, int e_newI);
    double ObsFunc(int oI_opt_agentI, int e_newI, int aI_opt_agentI);
    double Reward(int eI, int aI_opt_agentI);
    void ExportPOMDP();
    vector<int> DecomposeBestResponseState(int eI);
    vector<string> GetAllStates();
    vector<string> GetAllActions() {return this->Actions;};
    vector<string> GetAllObservations() {return this->Observations;};
    DecPomdpInterface* GetDecPomdpModel() {return this->DecPomdpModel;};
};


