/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

/* Only include this header file once. */
#ifndef _LOCALFSCNODE_H_
#define _LOCALFSCNODE_H_ 1

/* the include directives */
#include <iostream>
#include "../Interfaces/AlphaVector.h"
#include "../Interfaces/Belief.h"
#include "../Interfaces/BeliefSparse.h"


#define WHEREAMI std::cerr << __FILE__ << ":" << __func__ << ":" << __LINE__ << std::endl;
#define ARGH(x) { WHEREAMI; std::cerr << "MSG: " << x << std::endl; exit(1); }

using namespace std;

class LocalFscNode {
  AlphaVector best_alpha;
  Belief belief;
  BeliefSparse belief_sparse;
  size_t joint_action_index;
  size_t local_action_index;
  string description;
  int belief_counter = 1;
  double weight = 1.0; // used for weight merging
  double V;

 public:
  LocalFscNode(AlphaVector alpha, BeliefSparse& belief_sparse, size_t JAI, size_t AI){
    this->best_alpha = alpha;
    this->belief_sparse = belief_sparse;
    this->joint_action_index = JAI;
    this->local_action_index = AI;
    this->V = computeV();
  };

  LocalFscNode(){
    this->description = "InitNode";
  };
  ~LocalFscNode(){};
  void SetDescript(string s) {this->description = s;};
  string GetDescript() {return this->description;};
  size_t GetHumanAction() {return this->local_action_index;};
  size_t GetJointAction() {return this->joint_action_index;};
  size_t GetAction() {return this->GetHumanAction();};
  AlphaVector GetAlphaVector() {return this->best_alpha;};
  BeliefSparse GetJointBeliefSparse() {return this->belief_sparse;};
  double computeV(){
    double res = 0;
    map<int, double>::iterator it;
    for (it = this->belief_sparse.GetBeliefSparse()->begin(); it!= this->belief_sparse.GetBeliefSparse()->end(); it++)
    {
      res += best_alpha.GetValues()[it->first]*it->second;
    }
    return res;
  }

  // Better idea is to compare the alpha vector, need to test later
  bool operator<(const LocalFscNode & n) const {
    if(this->V < n.V){
      return true;
    }else
    {
      return false;
    }
    
  };

  // void MergeBelief(BeliefSparse& b){
  //   vector<double> merged_pb_states(this->belief.GetSize());
  //   map<int, double> _m_belief_sparse;
  //   for (size_t i = 0; i < this->belief.GetSize(); i++)
  //   {
  //     double pb_i = (this->belief[i]*belief_counter + b[i])/(belief_counter + 1);
  //     merged_pb_states[i] = pb_i;
  //     if (pb_i > 0)
  //     {
  //       _m_belief_sparse[i] = pb_i;
  //     }
      
  //   }
  //   Belief merged_belief(merged_pb_states);
  //   this->belief = merged_belief;
  //   this->belief_sparse.GetValues(_m_belief_sparse, merged_pb_states.size());
  //   this->belief_counter += 1;
  // };

  void MergeBelief(BeliefSparse& b){
    int Size = this->belief_sparse.GetSize();
    map<int, double> _m_belief_sparse;
    for (int i = 0; i < Size; i++)
    {
      double pb_i = (this->belief_sparse[i]*belief_counter + b[i])/(belief_counter + 1);
      // merged_pb_states[i] = pb_i;
      if (pb_i > 0)
      {
        _m_belief_sparse[i] = pb_i;
      }
      
    }
    this->belief_sparse.GetValues(_m_belief_sparse, Size);
    this->belief_counter += 1;
  };

  // void MergeBeliefWithWeights(BeliefSparse& b, double w_new){
  //   vector<double> merged_pb_states(this->belief.GetSize());
  //   double updated_weight = w_new + this->weight;
  //     map<int, double> _m_belief_sparse;

  //   for (size_t i = 0; i < this->belief.GetSize(); i++)
  //   {
  //     double pb_i = (this->belief[i]*(this->weight/updated_weight) + b[i]*(w_new/updated_weight));
  //     merged_pb_states[i] = pb_i;
  //     if (pb_i > 0)
  //     {
  //       _m_belief_sparse[i] = pb_i;
  //     }
  //   }
  //   Belief merged_belief(merged_pb_states);
  //   this->belief = merged_belief;
  //   this->belief_sparse.GetValues(_m_belief_sparse, merged_pb_states.size());
  //   this->weight = updated_weight;
  // };

    void MergeBeliefWithWeights(BeliefSparse& b, double w_new){
    int Size = this->belief_sparse.GetSize();
    double updated_weight = w_new + this->weight;
    map<int, double> _m_belief_sparse;

    for (int i = 0; i < Size; i++)
    {
      double pb_i = (this->belief_sparse[i]*(this->weight/updated_weight) + b[i]*(w_new/updated_weight));
      if (pb_i > 0)
      {
        _m_belief_sparse[i] = pb_i;
      }
    }
    this->belief_sparse.GetValues(_m_belief_sparse, Size);
    this->weight = updated_weight;
  };

};

#endif
