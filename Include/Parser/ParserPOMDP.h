/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#ifndef _PARSERPOMDP_H_
#define _PARSERPOMDP_H_ 1

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "../Interfaces/PomdpInterface.h"
#include <map>
#include <unordered_map>

using namespace std;

class ParsedPOMDP: public PomdpInterface
{
private:
    vector<string> States;
    vector<string> Actions;
    vector<string> Observations;
    int S_size; 
    int A_size; 
    int Obs_size;
    vector<double> b0;
    // vector<vector<vector<double>>> TransFuncVecs;

    // int Index = aI*S_size*S_size + sI*S_size + s_newI;
    vector<double> TransFuncVecs;
    // unordered_map<int,double> TransFuncVecs; // slower than vector

    // vector<vector<vector<double>>> ObsFuncVecs;
    // int Index = aI*S_size*Obs_size + sI*Obs_size + ObsI;
    vector<double> ObsFuncVecs;

    // vector<vector<double>> RewardFuncVecs;
    // index = aI*S_size + sI
    vector<double> RewardFuncVecs;

    double discount;

public:
    ParsedPOMDP(const string filename);
    ~ParsedPOMDP();
    double GetDiscount();
    int GetSizeOfS();
    int GetSizeOfA();
    int GetSizeOfObs();
    std::vector<double> GetInitBelief();
    double TransFunc(int sI, int aI, int s_newI);
    double ObsFunc(int oI, int s_newI, int aI);
    double Reward(int sI, int aI);
    vector<string> GetAllStates(){return this->States;};
    vector<string> GetAllActions() {return this->Actions;};
    vector<string> GetAllObservations() {return this->Observations;};
};



#endif

