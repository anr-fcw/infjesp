/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#ifndef _PARSERDECPOMDP_H_
#define _PARSERDECPOMDP_H_ 1

#include "../Interfaces/DecPomdpInterface.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>

using namespace std;

class ParsedDecPOMDP: public  DecPomdpInterface
{
    private:
        vector<string> States;
        vector<vector<string>> Actions;
        vector<vector<string>> Observations;
        int AgentsNb;
        int S_size; 
        vector<int> SizeActions;
        vector<int> SizeObservations;
        int JointA_size; 
        int JointObs_size;
        vector<double> b0;
        // vector<vector<vector<double>>> TransFuncVecs;
        // vector<vector<vector<double>>> ObsFuncVecs;
        // vector<vector<double>> RewardFuncVecs;
        vector<double> TransFuncVecs;
        vector<double> ObsFuncVecs;
        vector<double> RewardFuncVecs;


        map<vector<int>, int> m_IndividualToJointActionIndex;
        map<vector<int>, int> m_IndividualToJointObsIndex;
        map<int, vector<int>> m_JointToIndividualActionsIndices;
        map<int, vector<int>> m_JointToIndividualObsIndices;
        double discount;

    public:
        ParsedDecPOMDP(const string filename);
        ~ParsedDecPOMDP(){};
        virtual int GetNbAgents(){return this->AgentsNb;};
        double GetDiscount();
        int GetSizeOfS();
        int GetSizeOfJointA();
        int GetSizeOfJointObs();
        int GetSizeOfA(int agentI) {return this->Actions[agentI].size();};
        int GetSizeOfObs(int agentI) {return this->Observations[agentI].size();};
        vector<int> JointToIndividualActionsIndices(int JI); 
        vector<int> JointToIndividualObsIndices(int JI); 
        int IndividualToJointActionIndex(vector<int>& Indicies);
        int IndividualToJointObsIndex(vector<int>& Indicies);
        vector<vector<string>> GetAllActionsVecs();
        vector<vector<string>> GetAllObservationsVecs();
        vector<string> GetActionVec(int agentI);
        vector<string> GetObservationVec(int agentI);
        string GetActionName(int agentI, int aI);
        string GetObservationName(int agentI, int oI);
        vector<string> GetAllStates() {return this->States;};
        vector<double> GetInitBelief();
        double TransFunc(int sI, int JaI, int s_newI);
        double ObsFunc(int JoI, int s_newI, int JaI);
        double Reward(int sI, int JaI);
};



#endif