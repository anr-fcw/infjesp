/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include "../../third_party_dependencies/tinyxml/tinyxml.h"
#include <stdio.h>
#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;

void transformToMADPformat(string sarsop_path, string output_path);
