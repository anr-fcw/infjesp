/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#ifndef _FSCNODE_H_
#define _FSCNODE_H_

#include <iostream>

class FSCNode
{
private:
    /* data */
public:
    FSCNode(/* args */){};
    ~FSCNode(){};
    size_t GetAction();
};


#endif