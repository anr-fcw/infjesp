/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include "../Interfaces/DecPomdpInterface.h"
#include "../Interfaces/FSCBase.h"
#include "../BestResponseModelForAgentI/BestResponseModel.h"
#include "../BestResponseModelForAgentI/BestResponseMomdpModel.h"
#include "../BestResponseModelForAgentI/BestResponseMomdpModelSparse.h"
#include "../Utils/Utils.h"
#include "../SampleLocalPolicyFromMpomdp/SampleLocalFsc.h"
#include "../Parser/ParserSarsopResult.h"
#include "../Parser/ParserPOMDP.h"
#include "../Parser/ParserPOMDPSparse.h"
#include "../BuildPomdpFSC/BuildFSC.h"

using namespace std;

class InfJesp
{
private:
    // PomdpInterface* CurrentBestResponseModel;
    DecPomdpInterface* DecPomdpModel;
    double V_max;
    vector<FSCBase> FSCs;
    vector<double> V_fsc_history;
    vector<double> V_alphavecs_history;
    int OptimizingAgentIndex = -1;
    double error_gap;
    int formalization_type;
    int iter_max = 1000;


public:
    InfJesp(DecPomdpInterface* Pb, vector<FSCBase> FSCs_INIT, double error_gap, int formalization_type); // Init with FSCs
    InfJesp(DecPomdpInterface* Pb, int MaxNodesRandomInit,double error_gap, int formalization_type); // Init with random FSCs with a Max node size
    InfJesp(DecPomdpInterface* Pb,double error_gap, int init_type, int formalization_type); // Init with sampled local policy from MPOMDP
    InfJesp(DecPomdpInterface* Pb,double error_gap, int init_type, int sampled_policy_agent_index, int formalization_type); // Best Response for a selected agent with sampled policy from MPOMDP

    int GetNextAgentIndex();
    double Plan(int current_restart, ofstream& out);
    void PrintAllFSCs();
    void ExportMpomdpModel();
    vector<FSCBase> GetFSCs(){return this->FSCs;};
    ~InfJesp(){};

};

