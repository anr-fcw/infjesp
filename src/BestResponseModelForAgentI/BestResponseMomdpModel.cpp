/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include "../../Include/BestResponseMomdpModel.h"

// Compute the probability of having a local observation for a selected agent with a joint action
double BestResponseMomdpModel::ComputeSumPrObsAgentI(DecPomdpInterface *decpomdp, int optimizing_agentI, int oI, int s_newI, int JAI)
{
    double temp_pr_or = 0;
    int Index = s_newI * SizeJAI * SizeOfObs + JAI * SizeOfObs + oI;
    double res = _m_res_SumPrObsAgentI[Index];
    if (res >= 0)
    {
        return res;
    }

    for (unsigned int OI = 0; OI < this->_m_IndiciesOfObs.size(); OI++)
    {
        vector<int> ObsIndices = this->_m_IndiciesOfObs[OI];
        ObsIndices[optimizing_agentI] = oI;
        int JOI = decpomdp->IndividualToJointObsIndex(ObsIndices);
        temp_pr_or += decpomdp->ObsFunc(JOI, s_newI, JAI);
    }

    this->_m_res_SumPrObsAgentI[Index] = temp_pr_or;
    return temp_pr_or;
}

// Recursively build all combinations of all agents FSCs node possible indicies. NI <-> {n1,n2,n3...}
void BestResponseMomdpModel::RecursiveBuildAllFSCsNodesIndicies(int depth, int &NI, vector<int> NodesIndicies)
{
    if (depth != this->AgentNb)
    {
        // If the depth is the same with optimizing agent index, we just give a 0 node index (Because we won't use it)
        if (depth == this->CurrentOptimizingAgentI)
        {
            NodesIndicies.push_back(0);
            this->RecursiveBuildAllFSCsNodesIndicies(depth + 1, NI, NodesIndicies);
        }
        else
        {
            // Start with the start node
            // ni is the node index for the agent i (not the optimizing one), NI is the global index for node indicies vector
            for (int ni = 0; ni < this->FSCs[depth].GetNodesSize(); ni++)
            {
                NodesIndicies.push_back(ni);
                this->RecursiveBuildAllFSCsNodesIndicies(depth + 1, NI, NodesIndicies);
                NodesIndicies.pop_back();
            }
        }
    }
    else
    {
        // this->_m_IndiciesOfFSCsNodes[NI] = NodesIndicies;
        // this->_m_FSCsNodes_Indicies[NodesIndicies] = NI;
        this->_m_IndiciesOfFSCsNodes.push_back(NodesIndicies);

        NI += 1;
    }
};

// Same logic, Build all possible obs indicies
void BestResponseMomdpModel::RecursiveBuildAllObsIndicies(int depth, int &OI, vector<int> ObsIndicies)
{
    if (depth != this->AgentNb)
    {
        // If the depth is the same with optimizing agent index, we just give a 0 obs index (Because we won't use it)
        if (depth == this->CurrentOptimizingAgentI)
        {
            ObsIndicies.push_back(0);
            this->RecursiveBuildAllObsIndicies(depth + 1, OI, ObsIndicies);
        }
        else
        {
            // oi is the node index for the agent i (not the optimizing one), OI is the global index for node indicies vector
            for (int oi = 0; oi < this->DecPomdpModel->GetSizeOfObs(depth); oi++)
            {
                ObsIndicies.push_back(oi);
                this->RecursiveBuildAllObsIndicies(depth + 1, OI, ObsIndicies);
                ObsIndicies.pop_back();
            }
        }
    }
    else
    {
        // this->_m_IndiciesOfObs[OI] = ObsIndicies;
        this->_m_IndiciesOfObs.push_back(ObsIndicies);

        // this->_m_Obs_Indicies[ObsIndicies] = OI;
        OI += 1;
    }
};

double BestResponseMomdpModel::GetDiscount()
{
    return this->DecPomdpModel->GetDiscount();
};
int BestResponseMomdpModel::GetSizeOfS()
{
    // return the size of e
    return this->SizeOfS; // need to check if it is correct
};

int BestResponseMomdpModel::GetSizeOfStateSizeBeforeElimination()
{
    return this->ExtendedStateSizeBeforeElimination;
};

// Return the size of robot action space
int BestResponseMomdpModel::GetSizeOfA()
{
    return this->SizeOfA;
};

int BestResponseMomdpModel::GetSizeOfObs()
{
    return this->SizeOfObs;
};

std::vector<double> BestResponseMomdpModel::GetInitBelief()
{
    return this->b0;
};

vector<int> BestResponseMomdpModel::DecomposeBestResponseState(int eI)
{
    return this->_m_ExtendedStateIndicies[eI];
};

vector<string> BestResponseMomdpModel::GetAllStates()
{
    return this->States;
};

// !!! Need change
BestResponseMomdpModel::BestResponseMomdpModel(DecPomdpInterface *DecPomdp, vector<FSCBase> &FSCs, int optimizing_agentI)
{
    this->DecPomdpModel = DecPomdp;
    this->FSCs = FSCs;
    this->CurrentOptimizingAgentI = optimizing_agentI;
    this->Actions = DecPomdpModel->GetActionVec(optimizing_agentI);
    this->Observations = DecPomdpModel->GetObservationVec(optimizing_agentI);
    this->AgentNb = DecPomdp->GetNbAgents();

    // don't need init node anymore
    vector<int> Nodes_Indicies;
    vector<int> Obs_Indicies;
    int NI_start = 0;
    int OI = 0; // OI start

    this->RecursiveBuildAllFSCsNodesIndicies(0, NI_start, Nodes_Indicies);
    this->RecursiveBuildAllObsIndicies(0, OI, Obs_Indicies);

    this->NI_size = this->_m_IndiciesOfFSCsNodes.size();
    cout << "NI_size:" << this->NI_size << endl;
    this->SizeJAI = this->DecPomdpModel->GetSizeOfJointA();
    this->SizeDecPomdpStateSpace = this->DecPomdpModel->GetSizeOfS();
    this->SizeOfA = this->DecPomdpModel->GetSizeOfA(this->CurrentOptimizingAgentI);
    this->SizeOfObs = this->DecPomdpModel->GetSizeOfObs(this->CurrentOptimizingAgentI);
    this->ExtendedStateSizeBeforeElimination = this->DecPomdpModel->GetSizeOfS() * NI_size * SizeOfObs;
    vector<double> vec_sum_pr_obs_agentI(SizeOfObs * SizeDecPomdpStateSpace * SizeJAI, -1);
    this->_m_res_SumPrObsAgentI = vec_sum_pr_obs_agentI;

    int eI = 0; // extend state start with 0

    // first, create the b0 with extended state space, t = 0
    vector<double> decpomdp_b0 = this->DecPomdpModel->GetInitBelief();
    map<int, double> _m_BRM_b0;
    map<vector<int>, int> _m_extended_state_built_for_b0;

    bool built_b0 = false;      // To prevent built b0 with multiple times
    bool oI_init_valid = false; // To prevent built b0 with multiple times

    for (int oI_init = 0; oI_init < SizeOfObs; oI_init++)
    {
        // several states has prob at start
        for (int sI = 0; sI < this->DecPomdpModel->GetSizeOfS(); sI++)
        {
            if (decpomdp_b0[sI] > 0)
            {
                int JAI_init = 0;
                double SumPrObsAgentI = ComputeSumPrObsAgentI(DecPomdp, optimizing_agentI, oI_init, sI, JAI_init);

                if (SumPrObsAgentI > 0)
                {
                    oI_init_valid = true;
                    vector<int> temp_vec{sI, 0, oI_init};
                    this->_m_ExtendedStateIndicies.push_back(temp_vec);
                    string str_BRState = "s" + to_string(temp_vec[0]) + "N" + to_string(temp_vec[1]) + "o" + to_string(temp_vec[2]);
                    this->States.push_back(str_BRState);
                    _m_extended_state_built_for_b0[temp_vec] = 1;
                    if (!built_b0)
                    {
                        _m_BRM_b0[eI] = decpomdp_b0[sI];
                    }

                    eI += 1;
                }
            }
        }

        if (oI_init_valid)
        {
            built_b0 = true;
        }
    }

    // second, t > 0
    for (int sI = 0; sI < this->DecPomdpModel->GetSizeOfS(); sI++)
    {
        for (int oI = 0; oI < this->SizeOfObs; oI++)
        {
            // Need to elimate the impossible BestResponse states
            // 1. Eliminate the impossible oI, check in decpomdp model,
            // if sI can deduce a oI observation with any joint actions with opt agent actions
            bool oI_possible_obs_func = false;
            // bool OI_possible_obs_func = true;
            for (int JAI = 0; JAI < this->DecPomdpModel->GetSizeOfJointA(); JAI++)
            {
                double SumPrObsAgentI = ComputeSumPrObsAgentI(DecPomdp, optimizing_agentI, oI, sI, JAI);
                // This elimination method will still include impossible OI for SI and NI, because in some situations
                // OI is only possible with certain aI_opt_agent and NI, not all aI_opt with NI can obtain OI!
                if (SumPrObsAgentI > 0)
                // if (SumPrObsAgentI == 0)
                {
                    oI_possible_obs_func = true;
                    // OI_possible_obs_func = false;
                    break;
                }
            }

            if (oI_possible_obs_func)
            {
                for (int NI = 0; NI < NI_size; NI++)
                {
                    vector<int> temp_vec{sI, NI, oI};
                    if (_m_extended_state_built_for_b0.count(temp_vec) == 0)
                    {
                        this->_m_ExtendedStateIndicies.push_back(temp_vec);
                        string str_BRState = "s" + to_string(temp_vec[0]) + "N" + to_string(temp_vec[1]) + "o" + to_string(temp_vec[2]);
                        this->States.push_back(str_BRState);
                        eI += 1;
                    }
                }
            }
        }
    }

    cout << "Elimination Impossible States finished" << endl;
    this->SizeOfS = this->_m_ExtendedStateIndicies.size();

    // this->b0 = this->GetInitBelief();
    vector<double> BRM_belief_init(SizeOfS, 0);
    map<int, double>::iterator iter;
    for (iter = _m_BRM_b0.begin(); iter != _m_BRM_b0.end(); iter++)
    {
        BRM_belief_init[iter->first] = iter->second;
    }
    this->b0 = BRM_belief_init;
    cout << "The BRM have state size:" << this->SizeOfS << endl;

    // vector< vector< vector<double> > > T(SizeOfA, vector<vector<double> >(SizeOfS, vector<double>(SizeOfS,0) ));
    // // double T[actions.size()][States.size()][States.size()];
    // vector< vector< vector<double> > > O(SizeOfA, vector<vector<double> >(SizeOfS, vector<double>(SizeOfObs,0) ));
    // vector< vector<double> > R(SizeOfA, vector<double>(SizeOfS,0));

    // this->TransFuncVecs = T;
    // this->ObsFuncVecs = O;
    // this->RewardFuncVecs = R;

    cout << "BRM inited!" << endl;
}

vector<int> BestResponseMomdpModel::NItoActionsIndicies(int NI)
{
    vector<int> NodesInidices = this->_m_IndiciesOfFSCsNodes[NI];
    vector<int> act_indicies(this->AgentNb, 0);
    for (int agent_i = 0; agent_i < this->AgentNb; agent_i++)
    {
        if (agent_i == this->CurrentOptimizingAgentI)
        {
            continue;
        }
        act_indicies[agent_i] = this->FSCs[agent_i].GetActionIndexForNodeI(NodesInidices[agent_i]);
    }

    return act_indicies;
};

double BestResponseMomdpModel::ProbAllNodesTrans(int N_newI, int OI, int NI)
{
    double prob = 1;
    vector<int> NewNodesIndicies = this->_m_IndiciesOfFSCsNodes[N_newI];
    vector<int> ObsIndicies = this->_m_IndiciesOfObs[OI];
    vector<int> NodesIndicies = this->_m_IndiciesOfFSCsNodes[NI];

    for (int agent_i = 0; agent_i < this->AgentNb; agent_i++)
    {
        if (agent_i != this->CurrentOptimizingAgentI)
        {
            int n_newI_agent_i = NewNodesIndicies[agent_i];
            int oI_agent_i = ObsIndicies[agent_i];
            int nI_agent_i = NodesIndicies[agent_i];
            if (this->FSCs[agent_i].ProbTrans(nI_agent_i, oI_agent_i, n_newI_agent_i) > 0)
            {
                prob *= this->FSCs[agent_i].ProbTrans(nI_agent_i, oI_agent_i, n_newI_agent_i);
            }
            else
            {
                return 0;
            }
        }
        else
        {
            continue;
        }
    }

    return prob;
};

double BestResponseMomdpModel::TransFunc(int eI, int aI_opt_agentI, int e_newI)
{

    vector<int> indicies = this->_m_ExtendedStateIndicies[eI];
    vector<int> new_indicies = this->_m_ExtendedStateIndicies[e_newI];
    int sI = indicies[0];
    int NI = indicies[1];
    // int oI = indicies[2]; // not used
    int s_newI = new_indicies[0];
    int N_newI = new_indicies[1];

    vector<int> act_indicies = this->NItoActionsIndicies(NI); // this is the vector for actions indicies is at time t, deterministic

    int o_newI = new_indicies[2];

    act_indicies[this->CurrentOptimizingAgentI] = aI_opt_agentI;
    int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);

    // 1. Compute Pr(s'|s, aH,aR)
    double pr_s_new = 0;
    pr_s_new = this->DecPomdpModel->TransFunc(sI, JAI, s_newI);
    if (pr_s_new == 0)
    {
        return 0;
    }
    // 2. for all other agents'  observations
    // 2.1 Compute pr(n'| n, OI)
    // 2.2 Compute Pr(O | s, a)
    double sum_pr_obs_nodes = 0;
    for (unsigned int OI = 0; OI < this->_m_IndiciesOfObs.size(); OI++)
    {
        double pr_nodes = this->ProbAllNodesTrans(N_newI, OI, NI);
        vector<int> ObsIndices = this->_m_IndiciesOfObs[OI];
        ObsIndices[this->CurrentOptimizingAgentI] = o_newI;
        int JOI = this->DecPomdpModel->IndividualToJointObsIndex(ObsIndices);
        double pr_obs = this->DecPomdpModel->ObsFunc(JOI, s_newI, JAI);
        sum_pr_obs_nodes += pr_nodes * pr_obs;
    }

    double res = sum_pr_obs_nodes * pr_s_new;
    return res;
};

double BestResponseMomdpModel::ObsFunc(int oI_opt_agentI, int e_newI, int aI_opt_agentI)
{
    (void)(aI_opt_agentI);
    vector<int> new_indicies = this->_m_ExtendedStateIndicies[e_newI];
    int o_newI = new_indicies[2];
    if (oI_opt_agentI == o_newI)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

double BestResponseMomdpModel::Reward(int eI, int aI_opt_agentI)
{
    vector<int> indicies = this->_m_ExtendedStateIndicies[eI];
    int sI = indicies[0];
    int NI = indicies[1]; // this is the index for the nodes indicies at t
    double res = 0;

    vector<int> act_indicies = this->NItoActionsIndicies(NI); // this is the vector for actions indicies is at time t, deterministic
    act_indicies[this->CurrentOptimizingAgentI] = aI_opt_agentI;
    int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);

    res = this->DecPomdpModel->Reward(sI, JAI);

    return res;
};

void BestResponseMomdpModel::ExportPOMDP()
{

    string filename = "./TempFiles/BestResponseForAgentI.pomdp";
    ofstream fp(filename.c_str());
    int SizeA = this->GetSizeOfA();
    int SizeS = this->GetSizeOfS();
    int SizeO = this->GetSizeOfObs();

    // 1. discount
    fp << "discount: " << this->GetDiscount() << endl;
    // 2. values
    fp << "values: reward" << endl;
    // 3. states
    // Tricky part, need to consider what's the string for each state
    // fp 1. State at t 2. the node index label (Human action at t-1. optional) 3. Human Obs at t ?
    fp << "states: ";
    for (int eI = 0; eI < SizeS; eI++)
    {
        fp << this->States[eI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        fp << this->Actions[aRI] << " ";
    }
    fp << endl;
    // 5. observations
    fp << "observations: ";
    for (int oRI = 0; oRI < SizeO; oRI++)
    {
        fp << this->Observations[oRI] << " ";
    }
    fp << endl;
    // 6. start
    fp << "start: ";
    for (int eI = 0; eI < SizeS; eI++)
    {
        fp << this->b0[eI] << " ";
    }
    fp << endl;

    // All below only list none-zero prob items

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        for (int eI = 0; eI < SizeS; eI++)
        {
            for (int e_newI = 0; e_newI < SizeS; e_newI++)
            {
                // check prob is not 0
                double temp_pr = this->TransFunc(eI, aRI, e_newI);

                if (temp_pr > 0)
                {
                    // this->TransFuncVecs[aRI][eI][e_newI] = temp_pr;
                    fp << "T: " << aRI << " : " << eI << " : " << e_newI << " " << temp_pr << endl;
                }
            }
        }
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        for (int e_newI = 0; e_newI < SizeS; e_newI++)
        {

            vector<int> new_indicies = this->_m_ExtendedStateIndicies[e_newI];
            int oRI = new_indicies[2];

            fp << "O: " << aRI << " : " << e_newI << " : " << oRI << " " << 1 << endl;

            // for (int oRI = 0; oRI < SizeO; oRI++)
            // {
            //     // check prob is not 0
            //     double temp_pr = this->ObsFunc(oRI, e_newI, aRI);
            //     if (temp_pr>0)
            //     {
            //         // this->ObsFuncVecs[aRI][e_newI][oRI] = temp_pr;
            //         fp << "O: "<< aRI <<" : "<< e_newI <<" : " << oRI << " " << temp_pr << endl;
            //     }
            //     // sum_pr_o += temp_pr;
            // }

            // Found an extended state with a action where all observation are impossible, just add the first obs because this extended state will never be reached
            // if (sum_pr_o == 0){
            //     fp << "O: "<< aRI <<" : "<< e_newI <<" : " << 0 << " " << 1 << endl;
            // }
        }
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f
    for (int aI = 0; aI < SizeA; aI++)
    {
        for (int eI = 0; eI < SizeS; eI++)
        {
            float temp_r = this->Reward(eI, aI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r != 0 && !flag_nan)
            {
                // this->RewardFuncVecs[aI][eI] = temp_r;
                fp << "R: " << aI << " : " << eI << " : * : * " << temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }
            else
            {
            }
        }
    }
    fp.flush();
    fp.close();

    // this->AfterExport = true; // All the TranFunc, ObsFunc and Reward are stored in the self vectors
}