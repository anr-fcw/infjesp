/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include "../../Include/BestResponseModel.h"

// Compute the sum of probability of other agents' observations given a selected agent's observation
double BestResponseModel::ComputeSumPrObsAgentI(DecPomdpInterface *decpomdp, int optimizing_agentI, int OI, int s_newI, int JAI)
{
    double temp_pr_or = 0;
    int Index = s_newI * SizeJAI * SizeOI + JAI * SizeOI + OI;
    double res = _m_res_SumPrObsAgentI[Index];
    if (res >= 0)
    {
        return res;
    }
    vector<int> ObsIndices = this->_m_IndiciesOfObs[OI];

    for (int oI_newI = 0; oI_newI < decpomdp->GetSizeOfObs(optimizing_agentI); oI_newI++)
    {
        ObsIndices[optimizing_agentI] = oI_newI;
        int JOI = decpomdp->IndividualToJointObsIndex(ObsIndices);
        temp_pr_or += decpomdp->ObsFunc(JOI, s_newI, JAI);
    }

    this->_m_res_SumPrObsAgentI[Index] = temp_pr_or;
    return temp_pr_or;
}

// Recursively build all combinations of all agents FSCs node possible indicies. NI <-> {n1,n2,n3...}
void BestResponseModel::RecursiveBuildAllFSCsNodesIndicies(int depth, int &NI, vector<int> NodesIndicies)
{
    if (depth != this->AgentNb)
    {
        // If the depth is the same with optimizing agent index, we just give a 0 node index (Because we won't use it)
        if (depth == this->CurrentOptimizingAgentI)
        {
            NodesIndicies.push_back(0);
            this->RecursiveBuildAllFSCsNodesIndicies(depth + 1, NI, NodesIndicies);
        }
        else
        {
            // Start with the start node, init node already processed  !!!
            // ni is the node index for the agent i (not the optimizing one), NI is the global index for node indicies vector
            for (int ni = 1; ni < this->FSCs[depth].GetNodesSize(); ni++)
            {
                NodesIndicies.push_back(ni);
                this->RecursiveBuildAllFSCsNodesIndicies(depth + 1, NI, NodesIndicies);
                NodesIndicies.pop_back();
            }
        }
    }
    else
    {
        // this->_m_IndiciesOfFSCsNodes[NI] = NodesIndicies;
        // this->_m_FSCsNodes_Indicies[NodesIndicies] = NI;
        this->_m_IndiciesOfFSCsNodes.push_back(NodesIndicies);

        NI += 1;
    }
};

// Same logic, Build all possible obs indicies
void BestResponseModel::RecursiveBuildAllObsIndicies(int depth, int &OI, vector<int> ObsIndicies)
{
    if (depth != this->AgentNb)
    {
        // If the depth is the same with optimizing agent index, we just give a 0 obs index (Because we won't use it)
        if (depth == this->CurrentOptimizingAgentI)
        {
            ObsIndicies.push_back(0);
            this->RecursiveBuildAllObsIndicies(depth + 1, OI, ObsIndicies);
        }
        else
        {
            // oi is the node index for the agent i (not the optimizing one), OI is the global index for node indicies vector
            for (int oi = 0; oi < this->DecPomdpModel->GetSizeOfObs(depth); oi++)
            {
                ObsIndicies.push_back(oi);
                this->RecursiveBuildAllObsIndicies(depth + 1, OI, ObsIndicies);
                ObsIndicies.pop_back();
            }
        }
    }
    else
    {
        // this->_m_IndiciesOfObs[OI] = ObsIndicies;
        this->_m_IndiciesOfObs.push_back(ObsIndicies);

        // this->_m_Obs_Indicies[ObsIndicies] = OI;
        OI += 1;
    }
};

double BestResponseModel::GetDiscount()
{
    return this->DecPomdpModel->GetDiscount();
};
int BestResponseModel::GetSizeOfS()
{
    // return the size of e
    return this->SizeOfS; // need to check if it is correct
};

int BestResponseModel::GetSizeOfStateSizeBeforeElimination()
{
    return this->StateSizeBeforeElimination;
};

// Return the size of robot action space
int BestResponseModel::GetSizeOfA()
{
    return this->SizeOfA;
};

int BestResponseModel::GetSizeOfObs()
{
    return this->SizeOfObs;
};

std::vector<double> BestResponseModel::GetInitBelief()
{
    return this->b0;
};

vector<int> BestResponseModel::DecomposeBestResponseState(int eI)
{
    return this->_m_DecPomdpStateFscNodeObsIndicies[eI];
};

vector<string> BestResponseModel::GetAllStates()
{
    return this->States;
};

// !!! Need change
BestResponseModel::BestResponseModel(DecPomdpInterface *DecPomdp, vector<FSCBase> &FSCs, int optimizing_agentI)
{
    this->DecPomdpModel = DecPomdp;
    this->FSCs = FSCs;
    this->CurrentOptimizingAgentI = optimizing_agentI;
    this->Actions = DecPomdpModel->GetActionVec(optimizing_agentI);
    this->Observations = DecPomdpModel->GetObservationVec(optimizing_agentI);
    this->AgentNb = DecPomdp->GetNbAgents();
    // build map of indicies from (s,n,o) to (e)
    // build map of indicies from (e) to (s,n,o)

    vector<int> InitNodesIndicies(this->AgentNb, 0);
    // vector<int> InitObsIndicies(this->AgentNb, 0);

    // this->_m_IndiciesOfFSCsNodes[0] = InitNodesIndicies;
    this->_m_IndiciesOfFSCsNodes.push_back(InitNodesIndicies);

    // this->_m_FSCsNodes_Indicies[InitNodesIndicies] = 0;
    // this->_m_IndiciesOfObs[0] = InitObsIndicies;
    // this->_m_IndiciesOfObs.push_back(InitObsIndicies);

    // this->_m_Obs_Indicies[InitObsIndicies] = 0;

    int NI = 1; // NI start
    int OI = 0; // OI start
    vector<int> Nodes_Indicies;
    vector<int> Obs_Indicies;
    this->RecursiveBuildAllFSCsNodesIndicies(0, NI, Nodes_Indicies);
    this->RecursiveBuildAllObsIndicies(0, OI, Obs_Indicies);

    this->NI_size = this->_m_IndiciesOfFSCsNodes.size();
    this->SizeOI = this->_m_IndiciesOfObs.size();

    cout << "NI size:" << NI_size << endl;
    cout << "OI size:" << SizeOI << endl;

    this->SizeJAI = this->DecPomdpModel->GetSizeOfJointA();
    this->SizeDecPomdpStateSpace = this->DecPomdpModel->GetSizeOfS();
    this->StateSizeBeforeElimination = this->DecPomdpModel->GetSizeOfS() * NI_size * SizeOI;
    vector<double> vec_sum_pr_obs_agentI(SizeOI * SizeDecPomdpStateSpace * SizeJAI, -1);
    this->_m_res_SumPrObsAgentI = vec_sum_pr_obs_agentI;

    int eI = 0;
    // time t = 0, node is n0 with inital state
    // assign default ohI = 0
    int NI_init = 0; // we assign the init nodes vector index to 0

    // Need to find for the start sI, which OI_init is possible, not all OI is possible, be carefully!
    // int OI_init = 0;  This is a bad example, start state may be not possible to observe OI 0

    vector<double> decpomdp_b0 = this->DecPomdpModel->GetInitBelief();
    map<int, double> _m_BRM_b0;

    bool built_b0 = false;      // To prevent built b0 with multiple times
    bool OI_init_valid = false; // To prevent built b0 with multiple times

    for (int OI_init = 0; OI_init < SizeOI; OI_init++)
    {

        // several states has prob at start
        for (int sI = 0; sI < this->DecPomdpModel->GetSizeOfS(); sI++)
        {
            if (decpomdp_b0[sI] > 0)
            {
                int JAI_init = 0;
                // vector<int> SumPrObsIndex = {JAI_init, sI, OI_init};
                // vector<int> obs_indicies = this->_m_IndiciesOfObs[OI_init];
                // double SumPrObsAgentI = ComputeSumPrObsAgentI(DecPomdp, optimizing_agentI, obs_indicies, sI, JAI_init);
                double SumPrObsAgentI = ComputeSumPrObsAgentI(DecPomdp, optimizing_agentI, OI_init, sI, JAI_init);

                if (SumPrObsAgentI > 0)
                {
                    // this->_m_SumPrObs[SumPrObsIndex] = SumPrObsAgentI;
                    OI_init_valid = true;
                    vector<int> temp_vec{sI, NI_init, OI_init};
                    // int Index = sI*NI_size*SizeOI + NI_init*SizeOI + OI_init;
                    // this->_m_BestResponseStateIndicies[Index] = eI;
                    // this->_m_DecPomdpStateFscNodeObsIndicies[eI] = temp_vec;
                    this->_m_DecPomdpStateFscNodeObsIndicies.push_back(temp_vec);
                    string str_BRState = "s" + to_string(temp_vec[0]) + "N" + to_string(temp_vec[1]) + "O" + to_string(temp_vec[2]);
                    // string str_BRState = SetBestResponseStateName(this->DecPomdpModel, sI, this->CurrentOptimizingAgentI, InitNodesIndicies, InitObsIndicies);
                    // string str_BRState = str_BRState + "_" + str_indicies;
                    this->States.push_back(str_BRState);
                    if (!built_b0)
                    {
                        _m_BRM_b0[eI] = decpomdp_b0[sI];
                    }

                    eI += 1;
                }
            }
        }

        if (OI_init_valid)
        {
            built_b0 = true;
        }
    }

    // time t > 0, no more n0 for all agents !!!
    for (int sI = 0; sI < this->DecPomdpModel->GetSizeOfS(); sI++)
    {
        for (int NI = 1; NI < NI_size; NI++)
        {
            // for all the JAI with the current node n
            vector<int> act_indicies_without_opt_agent = this->NItoActionsIndicies(NI);
            for (int OI = 0; OI < SizeOI; OI++)
            {

                // Need to elimate the impossible BestResponse states
                // 1. Eliminate the impossible OI, check in decpomdp model,
                // if sI can deduce a OI observation with any joint actions with opt agent actions
                bool OI_possible_obs_func = false;
                // bool OI_possible_obs_func = true;

                for (int aI_opt_agent = 0; aI_opt_agent < DecPomdpModel->GetSizeOfA(optimizing_agentI); aI_opt_agent++)
                {
                    vector<int> act_indicies = act_indicies_without_opt_agent;
                    act_indicies[optimizing_agentI] = aI_opt_agent;
                    int JAI = DecPomdpModel->IndividualToJointActionIndex(act_indicies);
                    // only possible OHI will be used
                    // vector<int> SumPrObsIndex = {JAI, sI, OI};
                    // vector<int> obs_indicies = this->_m_IndiciesOfObs[OI];
                    // double SumPrObsAgentI = ComputeSumPrObsAgentI(DecPomdp, optimizing_agentI, obs_indicies, sI, JAI);
                    double SumPrObsAgentI = ComputeSumPrObsAgentI(DecPomdp, optimizing_agentI, OI, sI, JAI);

                    // This elimination method will still include impossible OI for SI and NI, because in some situations
                    // OI is only possible with certain aI_opt_agent and NI, not all aI_opt with NI can obtain OI!

                    if (SumPrObsAgentI > 0)
                    // if (SumPrObsAgentI == 0)
                    {
                        // this->_m_SumPrObs[SumPrObsIndex] = SumPrObsAgentI;
                        // testing why grid 3*3 dont work, NI = 0 is not checked???
                        // if (sI == 72 && NI == 3 &&  OI == 8)
                        // {
                        //     cout << "aI_opt_agent: " << aI_opt_agent << endl;
                        //     cout << "JAI " << JAI << " and OI " << OI << " is possible for s235 with prob:" <<  ComputeSumPrObsAgentI(DecPomdp, optimizing_agentI, obs_indicies, sI, JAI) << endl;
                        // }
                        // testing why grid 3*3 dont work

                        OI_possible_obs_func = true;
                        // OI_possible_obs_func = false;
                        break;
                    }
                }

                // 2. Eliminate the impossible OHI for node n
                bool OI_possible_node_trans = false;
                for (int N_newI = 0; N_newI < NI_size; N_newI++)
                {
                    // only possible OI will be used
                    // vector<int> PbAllNodesTransIndex = {N_newI, OI, NI};
                    double Pr_NodesTrans = this->ProbAllNodesTrans(N_newI, OI, NI);

                    if (Pr_NodesTrans > 0)
                    {
                        // this->_m_pb_AllNodesTrans[PbAllNodesTransIndex] = Pr_NodesTrans ;
                        OI_possible_node_trans = true;
                        break;
                    }
                }

                if (OI_possible_obs_func && OI_possible_node_trans)
                {
                    vector<int> temp_vec{sI, NI, OI};
                    // int Index = sI*NI_size*SizeOI + NI*SizeOI + OI;
                    // this->_m_BestResponseStateIndicies[Index] = eI;
                    // this->_m_DecPomdpStateFscNodeObsIndicies[eI] = temp_vec;
                    this->_m_DecPomdpStateFscNodeObsIndicies.push_back(temp_vec);

                    vector<int> ActionIndicies = this->NItoActionsIndicies(NI);
                    vector<int> ObsIndicies = this->_m_IndiciesOfObs[OI];

                    // int opt_agent = this->CurrentOptimizingAgentI;
                    // string str_indicies = "" + to_string(temp_vec[0]) + to_string(temp_vec[1]) + to_string(temp_vec[2]) ;
                    // string str_BRState = SetBestResponseStateName(this->DecPomdpModel, sI, this->CurrentOptimizingAgentI, ActionIndicies, ObsIndicies);
                    // str_BRState = str_BRState + "_" + str_indicies;
                    string str_BRState = "s" + to_string(temp_vec[0]) + "N" + to_string(temp_vec[1]) + "O" + to_string(temp_vec[2]);
                    this->States.push_back(str_BRState);
                    eI += 1;
                }
            }
        }
    }

    cout << "Elimination Impossible States finished" << endl;

    this->SizeOfS = this->_m_DecPomdpStateFscNodeObsIndicies.size();
    this->SizeOfA = this->DecPomdpModel->GetSizeOfA(this->CurrentOptimizingAgentI);
    this->SizeOfObs = this->DecPomdpModel->GetSizeOfObs(this->CurrentOptimizingAgentI);
    // this->b0 = this->GetInitBelief();
    vector<double> BRM_belief_init(SizeOfS, 0);
    map<int, double>::iterator iter;
    for (iter = _m_BRM_b0.begin(); iter != _m_BRM_b0.end(); iter++)
    {
        BRM_belief_init[iter->first] = iter->second;
    }
    this->b0 = BRM_belief_init;
    cout << "The BRM have state size:" << this->SizeOfS << endl;

    // vector< vector< vector<double> > > T(SizeOfA, vector<vector<double> >(SizeOfS, vector<double>(SizeOfS,0) ));
    // // double T[actions.size()][States.size()][States.size()];
    // vector< vector< vector<double> > > O(SizeOfA, vector<vector<double> >(SizeOfS, vector<double>(SizeOfObs,0) ));
    // vector< vector<double> > R(SizeOfA, vector<double>(SizeOfS,0));

    // this->TransFuncVecs = T;
    // this->ObsFuncVecs = O;
    // this->RewardFuncVecs = R;
    cout << "BRM inited!" << endl;
};

vector<int> BestResponseModel::NItoActionsIndicies(int NI)
{
    vector<int> NodesInidices = this->_m_IndiciesOfFSCsNodes[NI];
    vector<int> act_indicies(this->AgentNb, 0);
    for (int agent_i = 0; agent_i < this->AgentNb; agent_i++)
    {
        if (agent_i == this->CurrentOptimizingAgentI)
        {
            continue;
        }
        act_indicies[agent_i] = this->FSCs[agent_i].GetActionIndexForNodeI(NodesInidices[agent_i]);
    }

    return act_indicies;
};

double BestResponseModel::ProbAllNodesTrans(int N_newI, int OI, int NI)
{
    double prob = 1;
    vector<int> NewNodesIndicies = this->_m_IndiciesOfFSCsNodes[N_newI];
    vector<int> ObsIndicies = this->_m_IndiciesOfObs[OI];
    vector<int> NodesIndicies = this->_m_IndiciesOfFSCsNodes[NI];

    for (int agent_i = 0; agent_i < this->AgentNb; agent_i++)
    {
        if (agent_i != this->CurrentOptimizingAgentI)
        {
            int n_newI_agent_i = NewNodesIndicies[agent_i];
            int oI_agent_i = ObsIndicies[agent_i];
            int nI_agent_i = NodesIndicies[agent_i];
            if (this->FSCs[agent_i].ProbTrans(nI_agent_i, oI_agent_i, n_newI_agent_i) > 0)
            {
                prob *= this->FSCs[agent_i].ProbTrans(nI_agent_i, oI_agent_i, n_newI_agent_i);
            }
            else
            {
                return 0;
            }
        }
        else
        {
            continue;
        }
    }

    return prob;
};

double BestResponseModel::TransFunc(int eI, int aI_opt_agentI, int e_newI)
{
    // if (this->AfterExport)
    // {
    //     return this->TransFuncVecs[aI_opt_agentI][eI][e_newI];
    // }
    vector<int> indicies = this->_m_DecPomdpStateFscNodeObsIndicies[eI];
    vector<int> new_indicies = this->_m_DecPomdpStateFscNodeObsIndicies[e_newI];
    int sI = indicies[0];
    int NI = indicies[1]; // this is the index for the nodes indicies at t - 1
    int OI = indicies[2];
    int s_newI = new_indicies[0];
    int N_newI = new_indicies[1];                                 // this is node at t
    vector<int> act_indicies = this->NItoActionsIndicies(N_newI); // this is the vector for actions indicies is at time t, deterministic
    int O_newI = new_indicies[2];

    act_indicies[this->CurrentOptimizingAgentI] = aI_opt_agentI;
    int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);

    // 3. Compute Pr(s'|s, aH,aR)
    double pr_s_new = 0;
    pr_s_new = this->DecPomdpModel->TransFunc(sI, JAI, s_newI);
    if (pr_s_new == 0)
    {
        return 0;
    }
    // 2. Compute pr(n'| n, OI)
    // double pr_nodes = 0;
    // // pr_nodes = this->ProbAllNodesTrans(N_newI,OI,NI);
    // if (this->_m_pb_AllNodesTrans.count({N_newI,OI,NI}))
    // {
    //     pr_nodes = this->_m_pb_AllNodesTrans[{N_newI,OI,NI}];
    // }else
    // {
    //     pr_nodes = this->ProbAllNodesTrans(N_newI,OI,NI);
    // }

    double pr_nodes = this->ProbAllNodesTrans(N_newI, OI, NI);

    if (pr_nodes == 0)
    {
        return 0;
    }

    // 1. Compute the prob for having pr_OI
    double pr_sum_obs_opt_agent = ComputeSumPrObsAgentI(this->DecPomdpModel, this->CurrentOptimizingAgentI, O_newI, s_newI, JAI);
    if (pr_sum_obs_opt_agent == 0)
    {
        return 0;
    }

    double res = pr_sum_obs_opt_agent * pr_nodes * pr_s_new;
    return res;
};

double BestResponseModel::ObsFunc(int oI_opt_agentI, int e_newI, int aI_opt_agentI)
{
    // if (this->AfterExport)
    // {
    //     return this->ObsFuncVecs[aI_opt_agentI][e_newI][oI_opt_agentI];
    // }

    vector<int> new_indicies = this->_m_DecPomdpStateFscNodeObsIndicies[e_newI];
    int s_newI = new_indicies[0];
    int N_newI = new_indicies[1];                                 // this is the index for the nodes indicies at t
    vector<int> act_indicies = this->NItoActionsIndicies(N_newI); // this is the vector for actions indicies is at time t, deterministic
    int O_newI = new_indicies[2];
    vector<int> Obs_indicies = this->_m_IndiciesOfObs[O_newI];
    act_indicies[this->CurrentOptimizingAgentI] = aI_opt_agentI;
    int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);
    vector<int> SumPrObsIndex = {JAI, s_newI, O_newI};
    // double pr_sum_obs_opt_agent = 0;
    // if (this->_m_SumPrObs.count(SumPrObsIndex))
    // {
    //     pr_sum_obs_opt_agent = this->_m_SumPrObs[SumPrObsIndex];
    // }else
    // {
    //     // pr_sum_obs_opt_agent = ComputeSumPrObsAgentI(this->DecPomdpModel,this->CurrentOptimizingAgentI,Obs_indicies, s_newI,JAI);
    //     pr_sum_obs_opt_agent = ComputeSumPrObsAgentITest(this->DecPomdpModel,this->CurrentOptimizingAgentI,O_newI, s_newI,JAI);

    // }

    // double pr_sum_obs_opt_agent = this->_m_SumPrObs[SumPrObsIndex];
    // double pr_sum_obs_opt_agent = ComputeSumPrObsAgentI(this->DecPomdpModel,this->CurrentOptimizingAgentI,Obs_indicies, s_newI,JAI);

    Obs_indicies[this->CurrentOptimizingAgentI] = oI_opt_agentI;

    // if (aI_opt_agentI == 0 && e_newI == 0)
    // {
    //     cout << "!!! optimizing agent:" << this->CurrentOptimizingAgentI << endl;
    //     cout << "obs indicies: ";
    //     for (size_t i = 0; i < Obs_indicies.size(); i++)
    //     {
    //         cout << Obs_indicies[i] << " ";
    //     }
    //     cout << endl;
    //     cout << "!!! pr_sum_obs_opt_agent: " << pr_sum_obs_opt_agent << endl;
    // }

    int JOI = this->DecPomdpModel->IndividualToJointObsIndex(Obs_indicies);
    double pr_JOI = this->DecPomdpModel->ObsFunc(JOI, s_newI, JAI);

    // This if will hide the fact that if pr_JOI == 0 and sum_obs == 0, a nan will occur

    // if (pr_JOI > 0)
    // {
    //     double pr_sum_obs_opt_agent = ComputeSumPrObsAgentI(this->DecPomdpModel,this->CurrentOptimizingAgentI,O_newI, s_newI,JAI);
    //     return pr_JOI/pr_sum_obs_opt_agent;
    // }
    // return 0;

    // This method may give nan values when pr_JOI == 0 and sum_obs == 0
    double pr_sum_obs_opt_agent = ComputeSumPrObsAgentI(this->DecPomdpModel, this->CurrentOptimizingAgentI, O_newI, s_newI, JAI);
    return pr_JOI / pr_sum_obs_opt_agent;
};

double BestResponseModel::Reward(int eI, int aI_opt_agentI)
{
    vector<int> indicies = this->_m_DecPomdpStateFscNodeObsIndicies[eI];
    int sI = indicies[0];
    int NI = indicies[1]; // this is the index for the nodes indicies at t - 1
    int OI = indicies[2];
    double res = 0;
    // from the start node !! Not the init node
    for (int N_newI = 1; N_newI < NI_size; N_newI++)
    {
        vector<int> act_indicies = this->NItoActionsIndicies(N_newI); // this is the vector for actions indicies is at time t, deterministic
        act_indicies[this->CurrentOptimizingAgentI] = aI_opt_agentI;
        int JAI = this->DecPomdpModel->IndividualToJointActionIndex(act_indicies);
        double pb_all_nodes_trans = this->ProbAllNodesTrans(N_newI, OI, NI);

        if (pb_all_nodes_trans == 0)
        {
            continue;
        }
        else
        {
            res += pb_all_nodes_trans * this->DecPomdpModel->Reward(sI, JAI);
        }
    }
    return res;
};

void BestResponseModel::ExportPOMDP()
{
    string filename = "./TempFiles/BestResponseForAgentI.pomdp";
    ofstream fp(filename.c_str());
    int SizeA = this->GetSizeOfA();
    int SizeS = this->GetSizeOfS();
    int SizeO = this->GetSizeOfObs();

    // 1. discount
    fp << "discount: " << this->GetDiscount() << endl;
    // 2. values
    fp << "values: reward" << endl;
    // 3. states
    // Tricky part, need to consider what's the string for each state
    // fp 1. State at t 2. the node index label (Human action at t-1. optional) 3. Human Obs at t ?
    fp << "states: ";
    for (int eI = 0; eI < SizeS; eI++)
    {
        fp << this->States[eI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        fp << this->Actions[aRI] << " ";
    }
    fp << endl;
    // 5. observations
    fp << "observations: ";
    for (int oRI = 0; oRI < SizeO; oRI++)
    {
        fp << this->Observations[oRI] << " ";
    }
    fp << endl;
    // 6. start
    fp << "start: ";
    for (int eI = 0; eI < SizeS; eI++)
    {
        fp << this->b0[eI] << " ";
    }
    fp << endl;

    // All below only list none-zero prob items

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        for (int eI = 0; eI < SizeS; eI++)
        {
            for (int e_newI = 0; e_newI < SizeS; e_newI++)
            {
                // check prob is not 0
                double temp_pr = this->TransFunc(eI, aRI, e_newI);
                if (temp_pr > 0)
                {
                    // this->TransFuncVecs[aRI][eI][e_newI] = temp_pr;
                    fp << "T: " << aRI << " : " << eI << " : " << e_newI << " " << temp_pr << endl;
                }
            }
        }
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int aRI = 0; aRI < SizeA; aRI++)
    {
        for (int e_newI = 0; e_newI < SizeS; e_newI++)
        {
            double sum_pr_o = 0;
            for (int oRI = 0; oRI < SizeO; oRI++)
            {
                // check prob is not 0
                double temp_pr = this->ObsFunc(oRI, e_newI, aRI);

                if (temp_pr > 0)
                {
                    // this->ObsFuncVecs[aRI][e_newI][oRI] = temp_pr;
                    fp << "O: " << aRI << " : " << e_newI << " : " << oRI << " " << temp_pr << endl;
                }
                sum_pr_o += temp_pr;
            }

            // Found an extended state with a action where all observation are impossible, just add the first obs because this extended state will never be reached
            if (sum_pr_o == 0 || isnan(sum_pr_o))
            {

                fp << "O: " << aRI << " : " << e_newI << " : " << 0 << " " << 1 << endl;
            }
        }
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f
    for (int aI = 0; aI < SizeA; aI++)
    {
        for (int eI = 0; eI < SizeS; eI++)
        {
            float temp_r = this->Reward(eI, aI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r != 0 && !flag_nan)
            {
                // this->RewardFuncVecs[aI][eI] = temp_r;
                fp << "R: " << aI << " : " << eI << " : * : * " << temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }
            else
            {
            }
        }
    }
    fp.flush();
    fp.close();

    // this->AfterExport = true; // All the TranFunc, ObsFunc and Reward are stored in the self vectors
}