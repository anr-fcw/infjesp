/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include "../../Include/Parser/ParserDecPOMDP.h"

void BuildAllCombination(map<vector<int>, int> &IndToJoint_map, map<int, vector<int>> &JointToInd_map, vector<vector<string>> &input_space, vector<int> indicies, int depth);

ParsedDecPOMDP::ParsedDecPOMDP(const string filename)
{
    ifstream infile;
    infile.open(filename);
    if (!infile.is_open())
        cout << "open file failure" << endl;

    string temp;

    int actions_read_index = -1;
    int obs_read_index = -1;
    bool ReadActions = false;
    bool ReadObservations = false;
    bool ReadStart = false; // end at here
    // First Get agents number, discount and all the state, action and observation space
    while (getline(infile, temp) && !ReadStart)
    {
        istringstream is(temp);
        string s;
        int temp_num = 0;
        bool ReadAgents = false;
        bool ReadDiscount = false;
        bool ReadStates = false;
        while (is >> s)
        {

            if (s == "agents:")
            {
                ReadAgents = true;
            }

            else if (s == "discount:")
            {
                ReadDiscount = true;
            }
            else if (s == "states:")
            {
                ReadStates = true;
            }
            else if (s == "actions:")
            {
                ReadActions = true;
            }
            else if (s == "observations:")
            {
                ReadObservations = true;
            }
            else if (s == "start:")
            {
                ReadStart = true;
            }

            // Get Agents number， init Actions and Observations
            if (ReadAgents && temp_num == 1)
            {
                this->AgentsNb = stoi(s);
                // vector<vector<string>> A(this->AgentsNb);
                // vector<vector<string>> O(this->AgentsNb);
                this->Actions.resize(this->AgentsNb);
                this->Observations.resize(this->AgentsNb);
            }
            // Get discount factor
            if (ReadDiscount && temp_num == 1)
            {
                this->discount = stod(s);
            }
            // Get all the States
            if (ReadStates && temp_num > 0)
            {
                this->States.push_back(s);
            }
            // Get all actions
            // if (ReadActions && temp_num >0)
            if (ReadActions && actions_read_index < this->AgentsNb && actions_read_index > -1)
            {

                this->Actions[actions_read_index].push_back(s);
            }
            // Get all observations
            // if (ReadObservations && temp_num >0)
            if (ReadObservations && obs_read_index < this->AgentsNb && obs_read_index > -1)
            {
                this->Observations[obs_read_index].push_back(s);
            }
            // Get intial belief
            if (ReadStart && temp_num > 0)
            {
                b0.push_back(stod(s));
            }
            temp_num += 1;
        }

        if (ReadActions)
        {
            actions_read_index += 1;
        }
        if (ReadObservations)
        {
            obs_read_index += 1;
        }
    }
    infile.close();

    int temp_JA_size = 1;
    int temp_JO_size = 1;

    for (int i = 0; i < this->AgentsNb; i++)
    {
        temp_JA_size *= this->Actions[i].size();
        // cout << "Action vector size:" << this->Actions[i].size() << endl;
        temp_JO_size *= this->Observations[i].size();
    }

    this->JointA_size = temp_JA_size;
    this->JointObs_size = temp_JO_size;
    this->S_size = this->States.size();

    cout << "JointA Size:" << this->JointA_size << endl;
    cout << "JointObs Size:" << this->JointObs_size << endl;

    // Process the maps (J to I and I to J)
    BuildAllCombination(this->m_IndividualToJointActionIndex, this->m_JointToIndividualActionsIndices, this->Actions, {}, 0);
    BuildAllCombination(this->m_IndividualToJointObsIndex, this->m_JointToIndividualObsIndices, this->Observations, {}, 0);

    // vector< vector< vector<double> > > T(this->JointA_size, vector<vector<double> >(States.size(), vector<double>(States.size()) ));
    // vector< vector< vector<double> > > O(this->JointA_size, vector<vector<double> >(States.size(), vector<double>(this->JointObs_size) ));
    // vector< vector<double> > R(this->JointA_size, vector<double>(States.size()));
    this->TransFuncVecs.resize(JointA_size * S_size * S_size, 0);
    this->ObsFuncVecs.resize(JointA_size * S_size * JointObs_size, 0);
    this->RewardFuncVecs.resize(JointA_size * S_size, 0);

    infile.open(filename);
    // Get T,O and R
    while (getline(infile, temp))
    {
        // cout << temp << endl;
        istringstream is(temp);
        string s;
        int temp_num = 0;
        bool buildTrans = false;
        bool buildObs = false;
        bool buildReward = false;
        int aI = 0;
        int sI = 0;
        int oI = 0;
        int snewI = 0;
        double pb = 0;
        vector<int> act_indicies(this->AgentsNb);
        vector<int> obs_indicies(this->AgentsNb);
        // cout << endl;
        while (is >> s)
        {

            // cout << s <<" " ;

            // Get Transition Function
            if (s == "T:")
            {
                buildTrans = true;
            }
            else if (s == "O:")
            {
                buildObs = true;
            }
            else if (s == "R:")
            {
                buildReward = true;
            }

            if (0 < temp_num && temp_num < 1 + this->AgentsNb)
            {
                if (buildTrans || buildObs || buildReward)
                {
                    act_indicies[temp_num - 1] = stoi(s);
                }
            }
            else if (temp_num == 4)
            {
                if (buildTrans || buildObs || buildReward)
                {
                    sI = stoi(s);
                }
            }
            else if (5 < temp_num && temp_num < 6 + this->AgentsNb)
            {
                if (buildTrans)
                {
                    if (temp_num == 6)
                    {
                        snewI = stoi(s);
                    }
                }
                if (buildObs)
                {
                    obs_indicies[temp_num - 6] = stoi(s);
                }
            }
            else if (temp_num == 8)
            {
                // build T now
                if (buildTrans)
                {
                    pb = stod(s);
                    aI = this->IndividualToJointActionIndex(act_indicies);
                    int Index = aI * S_size * S_size + sI * S_size + snewI;
                    TransFuncVecs[Index] = pb;
                }
            }
            else if (temp_num == 9)
            {
                if (buildObs)
                {
                    pb = stod(s);
                    aI = this->IndividualToJointActionIndex(act_indicies);
                    oI = this->IndividualToJointObsIndex(obs_indicies);

                    int Index = aI * S_size * JointObs_size + sI * JointObs_size + oI;
                    ObsFuncVecs[Index] = pb;
                }
            }
            else if (temp_num == 10)
            {
                if (buildReward)
                {
                    pb = stod(s);
                    aI = this->IndividualToJointActionIndex(act_indicies);
                    int Index = aI * S_size + sI;
                    RewardFuncVecs[Index] = pb;
                }
            }

            temp_num += 1;
        }
    }

    infile.close();

    // this->TransFuncVecs = T;
    // this->ObsFuncVecs = O;
    // this->RewardFuncVecs = R;
}

void BuildAllCombination(map<vector<int>, int> &IndToJoint_map, map<int, vector<int>> &JointToInd_map, vector<vector<string>> &input_space, vector<int> indicies, int depth)
{
    if (depth != int(input_space.size()))
    {
        for (int i = 0; i < int(input_space[depth].size()); i++)
        {
            indicies.push_back(i);
            BuildAllCombination(IndToJoint_map, JointToInd_map, input_space, indicies, depth + 1);
            indicies.pop_back();
        }
    }
    else
    {
        int JI = 0;
        for (int i = 0; i < int(input_space.size()); i++)
        {
            int temp = 1;
            for (int j = i + 1; j < int(input_space.size()); j++)
            {
                temp *= input_space[j].size();
            }

            // cout << indicies[i]<< " " <<JI << endl;
            JI += indicies[i] * temp;
        }
        IndToJoint_map[indicies] = JI;
        JointToInd_map[JI] = indicies;
    }
}

int ParsedDecPOMDP::GetSizeOfS() { return this->S_size; };
int ParsedDecPOMDP::GetSizeOfJointA() { return this->JointA_size; };
int ParsedDecPOMDP::GetSizeOfJointObs() { return this->JointObs_size; };
vector<int> ParsedDecPOMDP::JointToIndividualActionsIndices(int JI) { return this->m_JointToIndividualActionsIndices[JI]; };
vector<int> ParsedDecPOMDP::JointToIndividualObsIndices(int JI) { return this->m_JointToIndividualObsIndices[JI]; };
int ParsedDecPOMDP::IndividualToJointActionIndex(vector<int> &Indicies) { return this->m_IndividualToJointActionIndex[Indicies]; };
int ParsedDecPOMDP::IndividualToJointObsIndex(vector<int> &Indicies) { return this->m_IndividualToJointObsIndex[Indicies]; };

vector<double> ParsedDecPOMDP::GetInitBelief() { return this->b0; };
double ParsedDecPOMDP::TransFunc(int sI, int JaI, int s_newI)
{
    int Index = JaI * S_size * S_size + sI * S_size + s_newI;
    return this->TransFuncVecs[Index];
    // return this->TransFuncVecs[JaI][sI][s_newI];
};

double ParsedDecPOMDP::ObsFunc(int JoI, int s_newI, int JaI)
{
    int Index = JaI * S_size * JointObs_size + s_newI * JointObs_size + JoI;
    return this->ObsFuncVecs[Index];
    // return this->ObsFuncVecs[JaI][s_newI][JoI];
};
double ParsedDecPOMDP::Reward(int sI, int JaI)
{
    int Index = JaI * S_size + sI;
    return this->RewardFuncVecs[Index];
    // return this->RewardFuncVecs[JaI][sI];
};

vector<vector<string>> ParsedDecPOMDP::GetAllActionsVecs() { return this->Actions; };
vector<vector<string>> ParsedDecPOMDP::GetAllObservationsVecs() { return this->Observations; };

double ParsedDecPOMDP::GetDiscount()
{
    return this->discount;
};

vector<string> ParsedDecPOMDP::GetActionVec(int agentI)
{
    return this->Actions[agentI];
};
vector<string> ParsedDecPOMDP::GetObservationVec(int agentI)
{
    return this->Observations[agentI];
};
string ParsedDecPOMDP::GetActionName(int agentI, int aI)
{
    return this->Actions[agentI][aI];
};
string ParsedDecPOMDP::GetObservationName(int agentI, int oI)
{
    return this->Observations[agentI][oI];
};