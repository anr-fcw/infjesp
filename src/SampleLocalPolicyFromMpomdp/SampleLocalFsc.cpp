/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include "../../Include/SampleLocalPolicyFromMpomdp/SampleLocalFsc.h"

using namespace std;


// compute a specified observation probability
double compute_pr_JOI(int LocalAgentIndex, int JOI, int Local_obsI, BeliefSparse b, int a, DecPomdpInterface* decpomdp){

    double temp_sum = 0;
    vector<int> current_obs = decpomdp->JointToIndividualObsIndices(JOI);
    if (current_obs[LocalAgentIndex]!= Local_obsI)
    {
        return 0;
    }
    


    double pr_JOI = compute_p_oba(JOI,b,a,decpomdp);
    for (int JOI_temp = 0; JOI_temp < decpomdp->GetSizeOfJointObs(); JOI_temp++)
    {
        vector<int> Obs_indicies = decpomdp->JointToIndividualObsIndices(JOI_temp);
        if (Obs_indicies[LocalAgentIndex] == Local_obsI)
        {
            temp_sum += compute_p_oba(JOI_temp,b,a,decpomdp);   

        }else
        {
            continue;
        }
        
        
    }

    return pr_JOI/temp_sum;
};


BeliefSparse* build_inital_belief_Sparse(DecPomdpInterface* decpomdp){
    return decpomdp->GetInitialBeliefSparse();
};




// Belief update
BeliefSparse BeliefUpdate(BeliefSparse b, int aI, int JOI,DecPomdpInterface* decpomdp){
    BeliefSparse b_new = Update(decpomdp, b, aI, JOI); 
    return b_new;

};

BeliefSparse BeliefUpdate(BeliefSparse& b, int aI, int JOI, double pr_oba, DecPomdpInterface* decpomdp){
    BeliefSparse b_new = Update(decpomdp, b, aI, JOI, pr_oba); 
    return b_new;

};

// vector<AlphaVector> HumanFSC::GetAlphaVectors(){
//     return this->AlphaVecs;
// };


int SampleLocalFsc::CheckAlphaExist(AlphaVector alpha){
    for (unsigned int i = 0; i < this->Nodes.size();i++)
    {
        // check two alpha vector equal or not
        AlphaVector current_alpha = this->Nodes[i].GetAlphaVector();
        if (current_alpha.GetValues() == alpha.GetValues() && current_alpha.GetActionIndex() == alpha.GetActionIndex())
        {
            return i;
        }
    }

    return -1;
    
};




// Stochasitic
void SampleLocalFsc::ProcessNodeStochastic(vector<AlphaVector>& alpha_vecs, int n_index, std::vector<LocalFscNode>& UnProcessedSet,DecPomdpInterface* decpomdp){
 
    LocalFscNode n = this->Nodes[n_index];
    size_t aI = n.GetJointAction();
    BeliefSparse b = n.GetJointBeliefSparse();
    for (int OHI = 0; OHI < decpomdp->GetSizeOfObs(LocalAgentIndex); OHI++)
    {

        // Deterministic local fsc !!!
        // int JOI_selected = 0;
        // double JOI_pr_max = 0;
        for (int JOI = 0; JOI < decpomdp->GetSizeOfJointObs(); JOI++)
        {
            double pr_or = compute_pr_JOI(LocalAgentIndex, JOI, OHI, b, aI,decpomdp);
            // cout <<"OHI: "<<OHI << ", JOI: "<<JOI <<", aI:" <<aI <<" ,Pr_or: " << pr_or << endl;

            // if (pr_or > JOI_pr_max)
            // {
            //     JOI_pr_max = pr_or;
            //     JOI_selected = JOI;
            // }


            BeliefSparse b_new;
            if (pr_or > 0)
            {
                b_new = BeliefUpdate(b,aI,JOI, decpomdp);
            }else
            {
                continue;
            }
            
            
            // b_new.PrintBelief();
            AlphaVector alpha_new = argmax_alpha_vector(alpha_vecs, &b_new);
            size_t new_aI = alpha_new.GetActionIndex();
            size_t new_aHI = decpomdp->JointToIndividualActionsIndices(new_aI)[LocalAgentIndex];
            LocalFscNode n_new(alpha_new, b_new, new_aI,new_aHI);
            // add a descript of action name
            n_new.SetDescript(decpomdp->GetAllActionsVecs()[LocalAgentIndex][new_aHI]);
            // Need to check if an alpha-vector is already exist in FSC
            int FLAG_Exist = CheckAlphaExist(alpha_new);
            if (FLAG_Exist == -1){
                this->Nodes.push_back(n_new);
                // cout << "New Processed Node Belief: ";
                // n_new.GetJointBelief().PrintBelief();
                // this->AlphaVecs.push_back(alpha_new);
                int n_new_index = this->Nodes.size() -1 ;
                // this->eta[n_index][OHI][n_new_index] = 1; 
                this->eta[n_index][OHI][n_new_index] = pr_or; 
                UnProcessedSet.push_back(n_new);
            }else
            {
                // should use the node with same alpha-vector not n_new (!!! IMPORTANT !!!)
                // Check what will happen if the link is not exist !!!
                // Maybe n_alpha exist but not with the link from n and OHI

                // node n_alpha_exist = this->Nodes[FLAG_Exist];
                // this->eta[n_index][OHI][FLAG_Exist] = 1;
                this->eta[n_index][OHI][FLAG_Exist] += pr_or;

                // Need to do next
                // Merge beliefs:
                // 1. By sum/2
                // 2. By weights
                this->Nodes[FLAG_Exist].MergeBelief(b_new);

            }




        }


            


       
         // Test!!! For solving the sarsop issue
        // Solutions for encounter undesirable oH:
        // 1. HumanFsc will back to init node
        // 2. Stay in the current node (Test this one)

        // Reward not generated after BestResponse ????
        bool undesirable_OHI = true;
        for (unsigned int new_I = 0; new_I<this->eta[n_index][OHI].size(); new_I++){
            if (this->eta[n_index][OHI][new_I] > 0){
                undesirable_OHI = false;
                break;
            }
        }
        
        if (undesirable_OHI){
            this->eta[n_index][OHI][n_index] = 1; // Point back to the current node
        }
        
    }
    
};


// Deterministic
void SampleLocalFsc::ProcessNodeDeterministic(vector<AlphaVector>& alpha_vecs, int n_index, std::vector<LocalFscNode>& UnProcessedSet,DecPomdpInterface* decpomdp){
 
    LocalFscNode n = this->Nodes[n_index];
    size_t aI = n.GetJointAction();
    BeliefSparse b = n.GetJointBeliefSparse();
    for (int OHI = 0; OHI < decpomdp->GetSizeOfObs(LocalAgentIndex); OHI++)
    {

        // Deterministic local fsc !!!
        int JOI_selected = 0;
        double JOI_pr_max = 0;
        for (int JOI = 0; JOI < decpomdp->GetSizeOfJointObs(); JOI++)
        {
            double pr_or = compute_pr_JOI(LocalAgentIndex, JOI, OHI, b, aI,decpomdp);
            // cout <<"OHI: "<<OHI << ", JOI: "<<JOI <<", aI:" <<aI <<" ,Pr_or: " << pr_or << endl;

            if (pr_or > 0 && pr_or > JOI_pr_max)
            {
                JOI_pr_max = pr_or;
                JOI_selected = JOI;
            }

        }
        double pr_oba = compute_p_oba(JOI_selected, b,aI,decpomdp);

        BeliefSparse b_new;
        if (pr_oba > 0)
        {
           b_new = BeliefUpdate(b,aI,JOI_selected,pr_oba, decpomdp);

        }else
        { 
            // it means we found a belief that this observation is impossible 
            // ! Linking back to itself !
            this->eta[n_index][OHI][n_index] = 1; // Point back to the current node
            continue;
        }

        // Belief b_new = BeliefUpdate(b,aI,JOI_selected, decpomdp);
        // Belief b_new = BeliefUpdate(b,aI,JOI_selected, decpomdp);
        // b_new.PrintBelief();
        AlphaVector alpha_new = argmax_alpha_vector(alpha_vecs, &b_new);
        size_t new_aI = alpha_new.GetActionIndex();
        size_t new_aHI = decpomdp->JointToIndividualActionsIndices(new_aI)[LocalAgentIndex];
        LocalFscNode n_new(alpha_new, b_new, new_aI,new_aHI);
        // add a descript of action name
        n_new.SetDescript(decpomdp->GetAllActionsVecs()[LocalAgentIndex][new_aHI]);
        // Need to check if an alpha-vector is already exist in FSC
        int FLAG_Exist = CheckAlphaExist(alpha_new);
        if (FLAG_Exist == -1){
            this->Nodes.push_back(n_new);
            // cout << "New Processed Node Belief: ";
            // n_new.GetJointBelief().PrintBelief();
            // this->AlphaVecs.push_back(alpha_new);
            int n_new_index = this->Nodes.size() -1 ;
            // this->eta[n_index][OHI][n_new_index] = 1; 
            this->eta[n_index][OHI][n_new_index] = 1; 
            UnProcessedSet.push_back(n_new);
        }else
        {
            // should use the node with same alpha-vector not n_new (!!! IMPORTANT !!!)
            // Check what will happen if the link is not exist !!!
            // Maybe n_alpha exist but not with the link from n and OHI

            // node n_alpha_exist = this->Nodes[FLAG_Exist];
            // this->eta[n_index][OHI][FLAG_Exist] = 1;
            this->eta[n_index][OHI][FLAG_Exist] =1;

            // Need to do next
            // Merge beliefs:
            // 1. Average merging
            // 2. By weights
            this->Nodes[FLAG_Exist].MergeBelief(b_new);

        }
            
       
         // Test!!! For solving the sarsop issue
        // Solutions for encounter undesirable oH:
        // 1. HumanFsc will back to init node
        // 2. Stay in the current node (Test this one)

        // Reward not generated after BestResponse ????
        bool undesirable_OHI = true;
        for (unsigned int new_I = 0; new_I<this->eta[n_index][OHI].size(); new_I++){
            if (this->eta[n_index][OHI][new_I] > 0){
                undesirable_OHI = false;
                break;
            }
        }
        
        if (undesirable_OHI){
            this->eta[n_index][OHI][n_index] = 1; // Point back to the current node
        }
        
    }
    
};


SampleLocalFsc::SampleLocalFsc(vector<AlphaVector> alpha_vecs, DecPomdpInterface* decpomdp, int LocalAgentIndex, int init_type, int formalization_type){
    // Initialize eta
    this->LocalAgentIndex = LocalAgentIndex;
    this->decpomdp = decpomdp;
    vector< vector< vector<double> > > eta_init(alpha_vecs.size()+1, 
        vector<vector<double> >(decpomdp->GetSizeOfObs(LocalAgentIndex), 
            vector<double>(alpha_vecs.size()+1 ) ));
    this->eta = eta_init;
    vector<LocalFscNode> UnProcessedSet; // Initlize a set to store the unprocessed nodes, Call it openlist
    BeliefSparse* b0 = build_inital_belief_Sparse(decpomdp);

    AlphaVector alpha0 = argmax_alpha_vector(alpha_vecs,b0);

    size_t aI = alpha0.GetActionIndex();
    size_t aHI = decpomdp->JointToIndividualActionsIndices(aI)[LocalAgentIndex];
    LocalFscNode n0(alpha0,*b0,aI,aHI);
    // add a descript of action name
    n0.SetDescript(decpomdp->GetAllActionsVecs()[LocalAgentIndex][aHI]);

    this->formalization_type = formalization_type;
    int n_index = 0; // start with n0
    int type = formalization_type;
    // commented for test momdp formalization
    if (type == 0){
        this->Nodes.push_back(n0);
    }else if(type == 1){
        InitNodeProcess(n0, decpomdp);
        n_index = 1; // start with n0
    }else{
        cerr << "Wrong argument for FSC type!"<<endl;
        throw("");
    }
    UnProcessedSet.push_back(n0);

    while (!UnProcessedSet.empty())
    {
        // std::vector<node>::iterator it = UnProcessedSet.begin();
        // node n = *it;
        // Attention, need to check if node n is still exist
        UnProcessedSet.erase(UnProcessedSet.begin());
        // cout << "UnProcessedSet Size: " << UnProcessedSet.size() << endl;

        // Need modify ProcessNode deterministic
        if (init_type == 1)
        {
            ProcessNodeStochastic(alpha_vecs,n_index,UnProcessedSet,decpomdp);
        }else if (init_type == 2)
        {
            ProcessNodeDeterministic(alpha_vecs,n_index,UnProcessedSet,decpomdp);
        }else{
            cout << "Wrong init type argument!"<< endl;
            throw("");
        }

        n_index++;
    }



}

void SampleLocalFsc::PrintGraph(DecPomdpInterface* decpomdp){
    cout  << endl;
    cout << " -------- " << endl;
    cout << "digraph agent_" << LocalAgentIndex <<" {" << endl;
    // define nodes in Graph
    for (unsigned int i = 0; i < this->Nodes.size(); i++){
        if (i == 0 && this->formalization_type == 1)
        {
        cout << "n"<< i << " [label = \" Init Node \"]"<< endl;
        }else{
        // cout << "n" <<i << "[label = \" aH:  " << this->Nodes[i].GetHumanAction() << ", a_joint:  " << this->Nodes[i].GetJointAction() << "\"]" <<endl;
        cout << "n" <<i << "[label = \" aH:  " << this->Nodes[i].GetDescript() << "\"]" <<endl;

        }

    }
    cout << endl;

    for (unsigned int n_newI = 0; n_newI < this->Nodes.size(); n_newI++){
        // node n_new = this->Nodes[i];
        for(int OHI = 0; OHI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); OHI++){
            for(unsigned int nI = 0; nI < this->Nodes.size(); nI++){
                // node n = this->Nodes[j];
                double pr_trans = this->eta[nI][OHI][n_newI];
                // Dont print self loops
                // if ( pr_trans > 0 )
                if ( pr_trans > 0 && nI != n_newI)
                {
                    
                
                    // decpomdp->GetObservation(HumanIndex, OHI)->GetName(); // Dont work
                    // decpomdp->GetObservationName(OHI, HumanIndex)  ; // Dont work
                     cout << "n"<< nI << " -> " << "n"<< n_newI <<"[label = \"oh: "<< OHI <<", pb: "<<pr_trans <<" \"]"<< endl;
                    // cout << "n"<< nI << " -> " << "n"<< n_newI <<"[label = \"oh: "<< decpomdp->GetAllObservationsVecs()[HumanIndex][OHI] <<", pb: "<<pr_trans <<" \"]"<< endl;

                   
                }
                
            }
        }
    }

    cout << "}" << endl;

}

vector<LocalFscNode> SampleLocalFsc::GetNodes(){
    return this->Nodes;
};

// be attention! n0 is the start node at b0, not init node which is node at t=-1
void SampleLocalFsc::InitNodeProcess(LocalFscNode n0,DecPomdpInterface* decpomdp){
    LocalFscNode InitNode;
    if (this->Nodes.size()!=0)
    {
        cout << "Error! Init Node must be added at beginning with Nodes size = 0!" << endl;
        throw("Error!");
    }
    
    this->Nodes.push_back(InitNode); // Index 0
    this->Nodes.push_back(n0); // Index 1
    for(int oI = 0; oI < decpomdp->GetSizeOfObs(this->LocalAgentIndex); oI++){
        this->eta[0][oI][1] = 1;
    }
    
}

void SampleLocalFsc::ExportFSC(){
    string filename = "MpomdpLocalPolicyAgent" + to_string(this->LocalAgentIndex) + ".fsc";
    ofstream fp(filename.c_str());
    fp << "nodes: ";
    int nI_start = 0;
    if (this->formalization_type == 1){
        nI_start = 1;
        fp << "99999 ";
    }
    for (unsigned int nI = nI_start; nI < this->Nodes.size(); nI++)
    {
        fp << this->Nodes[nI].GetAction()<< " ";
    }
    fp << endl;

    // T: obs_I : start-node_I : end-node_I %f
    for (int oI = 0; oI < this->decpomdp->GetSizeOfObs(this->LocalAgentIndex); oI++)
    {
        for (unsigned int nI = 0; nI < this->Nodes.size(); nI++)
        {
            for (unsigned int n_newI = 0; n_newI < this->Nodes.size(); n_newI++)
            {
                // check prob is not 0 
                double temp_pr = this->eta[nI][oI][n_newI];
                if (temp_pr>0)
                {
                    fp << "T: "<< oI <<" : "<< nI <<" : " << n_newI << " " << temp_pr << endl;
                }
                
            }
        }
    }
};