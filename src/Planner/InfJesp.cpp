/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include "../../Include/Planner/InfJesp.h"

// change sarsop timeout if needed
const string solver_path = "./third_party_dependencies/sarsop/src/pomdpsol --timeout 5";

InfJesp::InfJesp(DecPomdpInterface* Pb, vector<FSCBase> FSCs_INIT, double error_gap, int formalization_type){
    this->DecPomdpModel = Pb;
    this->FSCs = FSCs_INIT;
    this->V_max = -__DBL_MAX__;
    this->error_gap = error_gap;
    this->formalization_type = formalization_type;
    this->OptimizingAgentIndex = 0;
    for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
    {
        this->FSCs[i].PrintGraph(Pb, i);
    }
    

    cout << " --- InfJESP is initlized with given FSCs ---" << endl;
};

InfJesp::InfJesp(DecPomdpInterface* Pb, int MaxNodesRandomInit, double error_gap, int formalization_type){
    srand( clock() );

    this->DecPomdpModel = Pb;
    this->error_gap = error_gap;
    vector<FSCBase> FSCs_Random;
    this->formalization_type = formalization_type;
    this->OptimizingAgentIndex = 0;

    // formalization_type = 1
    // int formalization_type = 1;
    for (int i = 0; i < Pb->GetNbAgents(); i++)
    {
        FSCBase FSC_agenti_random(MaxNodesRandomInit, Pb->GetSizeOfA(i), Pb->GetSizeOfObs(i),formalization_type);
        FSCs_Random.push_back(FSC_agenti_random);
        string filename = "./TempFiles/TempFSCs/Agent" + to_string(i) + "Initial"; 
        FSC_agenti_random.ExportFSC(filename);
    }
    this->FSCs = FSCs_Random;
    this->V_max = -__DBL_MAX__;
    cout << " --- InfJESP is initlized with random FSCs ---" << endl;

};


InfJesp::InfJesp(DecPomdpInterface* Pb, double error_gap, int init_type, int formalization_type){
    this->DecPomdpModel = Pb;
    this->error_gap = error_gap;
    vector<FSCBase> FSCs_Random;
    // generate MPOMDP file
    this->ExportMpomdpModel();
    // solve the MPOMDP 
    string MpomdpPath = "MpomdpModel.pomdp";
    string command = solver_path + " -p " + to_string(error_gap) + " " + MpomdpPath;
    const char *p = command.data();
    system(p);
    cout << "SARSOP launched to solve the MPOMDP!" << endl;
    const string sarsop_res_path = "out.policy";
    const string output_mpomdp_res_path = "MpomdpAlphaVecsTemp";
    transformToMADPformat(sarsop_res_path, output_mpomdp_res_path);
    vector<AlphaVector> MpomdpAlphaVecs = ImportValueFunction(output_mpomdp_res_path);
    vector<FSCBase> FSCs;
    this->OptimizingAgentIndex = 0;

    this->formalization_type = formalization_type;

    // formalization_type = 1
    // int formalization_type = 1;
    // init_type = 1, stochastic
    // init_type = 2, deterministic

    // int init_type = 1;

    for (int i = 0; i < this->DecPomdpModel->GetNbAgents(); i++)
    {
        SampleLocalFsc* localfsc = new SampleLocalFsc(MpomdpAlphaVecs,Pb, i, init_type, formalization_type);
        localfsc->PrintGraph(Pb);
        localfsc->ExportFSC();

        delete localfsc;
        string localfscfile = "MpomdpLocalPolicyAgent" + to_string(i) + ".fsc";

        FSCBase builtlocalfsc(localfscfile, Pb->GetSizeOfObs(i), formalization_type);
        FSCs.push_back(builtlocalfsc);
    }



    this->FSCs = FSCs;
    this->V_max = -__DBL_MAX__;
    cout << " --- InfJESP is initlized with sampled local policy from MPOMDP result ---" << endl;

};

InfJesp::InfJesp(DecPomdpInterface* Pb,double error_gap, int init_type, int sampled_policy_agent_index, int formalization_type){
    this->DecPomdpModel = Pb;
    this->error_gap = error_gap;
    vector<FSCBase> FSCs_Random;
    // generate MPOMDP file
    this->ExportMpomdpModel();
    // solve the MPOMDP 
    string MpomdpPath = "MpomdpModel.pomdp";
    string command = solver_path + " -p " + to_string(error_gap) + " " + MpomdpPath;
    const char *p = command.data();
    system(p);
    cout << "SARSOP launched to solve the MPOMDP!" << endl;
    const string sarsop_res_path = "out.policy";
    const string output_mpomdp_res_path = "MpomdpAlphaVecsTemp";
    transformToMADPformat(sarsop_res_path, output_mpomdp_res_path);
    vector<AlphaVector> MpomdpAlphaVecs = ImportValueFunction(output_mpomdp_res_path);
    vector<FSCBase> FSCs(this->DecPomdpModel->GetNbAgents());

    this->OptimizingAgentIndex = sampled_policy_agent_index;
    this->iter_max = 1;

    this->formalization_type = formalization_type;



    SampleLocalFsc* localfsc = new SampleLocalFsc(MpomdpAlphaVecs,Pb, sampled_policy_agent_index, init_type, formalization_type);
    localfsc->PrintGraph(Pb);
    localfsc->ExportFSC();
    delete localfsc;
    string localfscfile = "MpomdpLocalPolicyAgent" + to_string(sampled_policy_agent_index) + ".fsc";
    FSCBase builtlocalfsc(localfscfile, Pb->GetSizeOfObs(sampled_policy_agent_index), formalization_type);
    FSCs[sampled_policy_agent_index] = builtlocalfsc;
    

    this->FSCs = FSCs;
    this->V_max = -__DBL_MAX__;
    cout << " --- InfJESP is initlized with sampled local policy from MPOMDP result ---" << endl;

};

int InfJesp::GetNextAgentIndex() {
    int next_agentI = (this->OptimizingAgentIndex+1) % this->DecPomdpModel->GetNbAgents();
    return(next_agentI);
}

double InfJesp::Plan(int current_restart, ofstream& out){
    // clock_t PlanStartTime,PlanEndTime;
    // PlanStartTime = clock(); // Use system time, need to fix
    time_t PlanStartTime,PlanEndTime;
    PlanStartTime = time(NULL);

    int iter = 0;
    int nr_non_improving_agents = 0;
    vector<string> log_res;


    // ------------ Main Algo of InfJesp -------------------
    while (nr_non_improving_agents < this->DecPomdpModel->GetNbAgents() -1 && iter < this->iter_max)
    {
        clock_t startTime,endTime;
        startTime = clock();

        int StateSizeBeforeElimination = -1 ;
        int StateSize = -1;
        this->OptimizingAgentIndex = this->GetNextAgentIndex(); 



        cout << "Building the BestResponse Model" << endl;
        if (formalization_type == 0){

            // 1. ----- First Build the BRM, momdp formalization ---------
            BestResponseMomdpModelSparse* bestresponseMomdpPomdp = new BestResponseMomdpModelSparse(this->DecPomdpModel, this->FSCs, this->OptimizingAgentIndex);
            cout << "BestResponseModel built! Exporting the BestResponseModel" << endl;
            bestresponseMomdpPomdp->ExportPOMDP();
            endTime = clock();//
            StateSizeBeforeElimination = bestresponseMomdpPomdp->GetSizeOfStateSizeBeforeElimination();
            StateSize = bestresponseMomdpPomdp->GetSizeOfS();
            // Read the BRM file and use this POMDP is much faster
            delete bestresponseMomdpPomdp;
            bestresponseMomdpPomdp = nullptr;
        }else if(formalization_type == 1) {
            // 1. ----- First Build the BRM, "init node" formalization ---------
            // BestReponseModel Need correction!!!


            // BestResponseModel* bestresponsePomdp = new BestResponseModel(this->DecPomdpModel, this->FSCs, this->OptimizingAgentIndex);
            // // BestResponseModel bestresponsePomdp(this->DecPomdpModel, this->FSCs, this->OptimizingAgentIndex);
            // cout << "BestResponseModel built! Exporting the BestResponseModel" << endl;
            // bestresponsePomdp->ExportPOMDP();
            // // bestresponsePomdp.ExportPOMDP();
            // endTime = clock();//
            // StateSizeBeforeElimination = bestresponsePomdp->GetSizeOfStateSizeBeforeElimination();
            // StateSize = bestresponsePomdp->GetSizeOfS();
            // // Read the BRM file and use this POMDP is much faster
            // delete bestresponsePomdp;
            // bestresponsePomdp = nullptr;


        }else{
            cerr << "Wrong formalization type!"<<endl;
            throw("");
        }


   




        // Add a pause, and see if there is a difference in memory using
        // PomdpInterface *BRMPb = new ParsedPOMDP("BestResponseForAgentI.pomdp");
        PomdpInterface *BRMPb = new ParsedPOMDPSparse("./TempFiles/BestResponseForAgentI.pomdp");

        // cout << "The run time for export BestResponsePOMDP model is: " <<(double)(endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;
        cout << "BestResponseModel exported!" << endl;
        // ------------------------------------------------------

        // 2. -------- Solve the BRM with SARSOP --------------
        string file_path = "TempFiles/BestResponseForAgentI.pomdp";
        string command = solver_path + " -p " + to_string(error_gap) + " " + file_path;
        const char *p = command.data();
        system(p);
        // ------------------------------------------------------

        // 3. --- Read the SARSOP result and tranform to the MADP format -----
        string sarsop_res_path = "out.policy";
        string output_res_path = "AlphaVecsTemp";
        transformToMADPformat(sarsop_res_path, output_res_path);
        vector<AlphaVector> AlphaVecs = ImportValueFunction(output_res_path);

        // ------------------------------------------------------


        // 4. -------------- Policy Evaluation -----------------
        // double V_alphavecs = EvaluationWithAlphaVecs(&bestresponsePomdp, AlphaVecs);  // ADD also this value to the logs result
        double V_alphavecs = EvaluationWithAlphaVecs(BRMPb, AlphaVecs);  // ADD also this value to the logs result

        cout << "Building the FSC for agent " << this->OptimizingAgentIndex << endl;

        FSC fsc(AlphaVecs,BRMPb, formalization_type, error_gap); 
        // FSC fsc(AlphaVecs,&bestresponsePomdp); 
        cout << "Built the FSC for agent " << this->OptimizingAgentIndex << endl;
        double V_fsc = fsc.PolicyEvaluation(); //!!! Takes time !!!
        this->V_fsc_history.push_back(V_fsc);
        // ------------------------------------------------------

        // 5. ------------ Value Improvment Check -------------
        if (V_fsc > this->V_max)
        // if (V_alphavecs > this->V_max)
        {
            cout << " --- New Best Policy Found, V:" << V_fsc <<" --- " << endl; 
            string filename = "./TempFiles/TempFSCs/Agent" + to_string(this->OptimizingAgentIndex) + "Round" + to_string(iter); 
            fsc.ExportFSC(filename);
            FSCBase FSC_agentI = FSCBase(filename, this->DecPomdpModel->GetSizeOfObs(this->OptimizingAgentIndex), formalization_type);
            this->FSCs[this->OptimizingAgentIndex] = FSC_agentI;
            this->V_max = V_fsc;
            // this->V_max = V_alphavecs;
            this->V_alphavecs_history.push_back(V_alphavecs);
            nr_non_improving_agents = 0;
        }
        else{
            nr_non_improving_agents++;
        }

        endTime = clock();//


        

        iter += 1;

        // ------------------------------------------------------
        // Delete BRM to free the memory, because it's slow to directly use this model
        delete BRMPb;

        double iter_time = (double)(endTime - startTime) / CLOCKS_PER_SEC;

        // log result
        if (V_fsc >= this->V_max)
        // if (V_alphavecs > this->V_max)
        {
        out << current_restart <<"," <<iter<< ","<<this->OptimizingAgentIndex<<","<<fsc.GetNodesSize()<<","
        <<StateSizeBeforeElimination<<","<<StateSize<<","<<V_fsc<<","<<V_alphavecs<<","<<iter_time <<endl;
        // should not contain the last iteration's information if there is no improvement (decerase of value)
        }
    }
    // ------------------------------------------------------

    // Finished planning
    // store the time
    // PlanEndTime = clock();//计时结束
    PlanEndTime = time(NULL);
    double plantime = (double)(PlanEndTime - PlanStartTime) ;
    out << " , , , , , , , , ,"<< this->V_max<<"," << plantime <<"," << this->FSCs[0].GetNodesSize() << "," <<  this->FSCs[1].GetNodesSize() <<"," << iter <<endl;
    

    
    this->PrintAllFSCs();
    
    cout << "--- INF-JESP Plan Finished --- " << endl;
    cout << "V fsc final: " << this->V_max << endl;
    cout << "V fsc history: ";
    for (int i = 0; i < int(this->V_fsc_history.size()); i++)
    {
        cout << this->V_fsc_history[i] <<",";
    }
    cout << endl;

    cout << "V alphavecs history: ";
    for (int i = 0; i < int(this->V_alphavecs_history.size()); i++)
    {
        cout << this->V_alphavecs_history[i] <<",";
    }
    cout << endl;

    return this->V_max;

};

void InfJesp::PrintAllFSCs(){
    for (int i = 0; i < int(this->FSCs.size()); i++)
    {
        cout << " ---- FSC for agent " << i << " ---- " << endl;
        this->FSCs[i].PrintGraph(this->DecPomdpModel, i);
    }
};

void InfJesp::ExportMpomdpModel(){
    string filename = "MpomdpModel.pomdp";
    ofstream fp(filename.c_str());
    // 1. discount
    fp << "discount: "<< this->DecPomdpModel->GetDiscount() << endl;
    // 2. values
    fp << "values: reward"<<endl;

    // 3. states
    fp << "states: ";
    for (int sI = 0; sI < this->DecPomdpModel->GetSizeOfS(); sI++)
    {
        fp << this->DecPomdpModel->GetAllStates()[sI] << " ";
    }
    fp << endl;

    // 4. actions
    fp << "actions: ";
    for (int JaI = 0; JaI < this->DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        vector<int> actions_indicies = this->DecPomdpModel->JointToIndividualActionsIndices(JaI); 
        string JointActionName = "";
        for (int i = 0; i < int(actions_indicies.size()); i++)
        {
            JointActionName += this->DecPomdpModel->GetActionName(i, actions_indicies[i]);
        }
        fp << JointActionName << " ";
    }
    fp << endl;

    // 5. observations
    fp << "observations: ";
    for (int JoI = 0; JoI < this->DecPomdpModel->GetSizeOfJointObs(); JoI++)
    {
        vector<int> obs_indicies = this->DecPomdpModel->JointToIndividualObsIndices(JoI); 
        string JointObsName = "";
        for (int i = 0; i < int(obs_indicies.size()); i++)
        {
            JointObsName += this->DecPomdpModel->GetObservationName(i, obs_indicies[i]);
        }
        fp << JointObsName << " ";
    }
    fp << endl;

    // 6. start
    fp << "start: ";
    map<int, double>* b0_sparse = this->DecPomdpModel->GetInitialBeliefSparse()->GetBeliefSparse();

    for (int sI = 0; sI < this->DecPomdpModel->GetSizeOfS(); sI++)
    {
        double value = 0;
        map<int, double>::iterator it;
        it = b0_sparse->find(sI);
        if (it != b0_sparse->end())
        {
            value = it->second;
        }
        fp << value << " ";

    }
    fp << endl;

    // 7. T
    // T: action_I : start-state_I : end-state_I %f
    for (int JaI = 0; JaI < this->DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < this->DecPomdpModel->GetSizeOfS(); sI++)
        {
            for (int s_newI = 0; s_newI < this->DecPomdpModel->GetSizeOfS(); s_newI++)
            {
                // check prob is not 0 
                double temp_pr = this->DecPomdpModel->TransFunc(sI, JaI, s_newI);
                if (temp_pr>0)
                {
                    fp << "T: "<< JaI <<" : "<< sI <<" : " << s_newI << " " << temp_pr << endl;
                }
                
            }
            
        }
        
    }

    // 8. O
    // O: action_I : end-state_I : observation_I %f
    for (int JaI = 0; JaI < this->DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int s_newI = 0; s_newI < this->DecPomdpModel->GetSizeOfS(); s_newI++)
        {
            for (int JoI = 0; JoI < this->DecPomdpModel->GetSizeOfJointObs(); JoI++)
            {
                // check prob is not 0 
                double temp_pr = this->DecPomdpModel->ObsFunc(JoI, s_newI, JaI);
                if (temp_pr>0)
                {
                    fp << "O: "<< JaI <<" : "<< s_newI <<" : " << JoI << " " << temp_pr << endl;
                }
                
            }
            
        }
        
    }

    // 9. R
    // R: action_I : start-state_I : * : * %f   
    for (int JaI = 0; JaI < this->DecPomdpModel->GetSizeOfJointA(); JaI++)
    {
        for (int sI = 0; sI < this->DecPomdpModel->GetSizeOfS(); sI++)
        {
            float temp_r = this->DecPomdpModel->Reward(sI, JaI);
            bool flag_nan = isnan(temp_r);
            // cout << temp_r << endl; // have this, the output is oK, without this gonna no R, strange
            if (temp_r!=0 && !flag_nan)
            {
                fp << "R: "<< JaI << " : "<< sI <<" : * : * "<< temp_r << endl;
                // fp << "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
                // cerr <<"CERR: "<< "R: "<< aRI << " : "<< eI <<" : * : * "<< temp_r <<" " << flag_nan <<" "<< (temp_r!=0) << " "<< (temp_r!=0 && !flag_nan) << endl;
            }else{
            }
        }
    }
    fp.flush();
    fp.close();        
    

};