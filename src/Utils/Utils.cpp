/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include "../../Include/Utils/Utils.h"
#include <cmath>
#include <numeric>

bool CheckAlphaExist(vector<AlphaVector> &a_vecs, AlphaVector &alpha)
{
    if (a_vecs.size() == 0)
        return false;
    for (int i = 0; i < int(a_vecs.size()); i++)
    {
        if (a_vecs[i] == alpha)
        {
            return true;
        }
    }
    return false;
}

void PrintAlphaVectors(vector<AlphaVector> &a_vecs)
{
    for (int i = 0; i < int(a_vecs.size()); i++)
    {
        a_vecs[i].Print();
    }
};

void PrintVector(vector<double> &V)
{
    for (int i = 0; i < int(V.size()); i++)
    {
        cout << V[i] << " ";
    }
    cout << endl;
};

void PrintAllAlphaAOVecs(vector<vector<vector<AlphaVector>>> &a_ao_vecs)
{
    // For all the action index in alpha_a_b
    for (int aI = 0; aI < int(a_ao_vecs.size()); aI++)
    {
        // cout << " ---- aI:"<<aI;
        // For all the oI
        for (int oI = 0; oI < int(a_ao_vecs[aI].size()); oI++)
        {
            // cout << ", oI:"<<oI <<" ---- " << endl;
            // For all alpha idx
            for (int idx = 0; idx < int(a_ao_vecs[aI][oI].size()); idx++)
            {
                cout << " ---- aI:" << aI << ", oI:" << oI << ", alpha_idx:" << idx << " -----" << endl;
                a_ao_vecs[aI][oI][idx].Print();
            }
        }
    }
};

// This function is used for selected the optimal action given a current belief and alpha-vectors

AlphaVector argmax_alpha_vector(vector<AlphaVector> &alpha_vecs, BeliefSparse *b)
{
    // initilized a with -1, later can check if an action is successfully selected
    AlphaVector a_max_val;
    double v_max = -DBL_MAX;

    map<int, double> *belief_sparse = b->GetBeliefSparse();
    map<int, double>::iterator it;

    for (int i = 0; i < int(alpha_vecs.size()); ++i)
    {
        vector<double> alpha_temp = alpha_vecs[i].GetValues();
        if (int(alpha_temp.size()) != b->GetSize())
        {

            cerr << "belief state size:" << b->GetSize() << endl;
            cerr << "alpha state size:" << alpha_temp.size() << endl;
            cerr << "State dimensions between the given alpha-vector and belief don't match!" << endl;
            throw(("State dimensions between the given alpha-vector and belief don't match!"));
        }
        double v_temp = 0;

        for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
        {
            v_temp += alpha_temp[it->first] * (it->second);
        }

        // for (int j = 0; j < int(alpha_temp.size()); ++j){
        //     v_temp += alpha_temp[j]*b[j];
        // }

        if (v_temp > v_max)
        {
            a_max_val = alpha_vecs[i];
            v_max = v_temp;
        }
    }
    if (a_max_val.GetValues().size() == 0)
    {
        cerr << "alpha_vecs.size()=" << alpha_vecs.size() << endl;
        throw("argmax_alpha not found");
    }
    return a_max_val;
}

double EvaluationWithAlphaVecs(PomdpInterface *Pb, vector<AlphaVector> &alpha_vecs)
{
    double v_max = -DBL_MAX;
    map<int, double> *belief_sparse = Pb->GetInitBeliefSparse();
    map<int, double>::iterator it;

    for (int i = 0; i < int(alpha_vecs.size()); ++i)
    {
        vector<double> alpha_temp = alpha_vecs[i].GetValues();
        double v_temp = 0;
        for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
        {
            v_temp += alpha_temp[it->first] * (it->second);
        }

        if (v_temp > v_max)
        {
            v_max = v_temp;
        }
    }
    return v_max;
};

int argmax_alpha(vector<AlphaVector> &alpha_vecs, BeliefSparse &b)
{
    // cout << " ------ Current Belief ---------"<<endl;
    // b.PrintBelief();
    double v_max = -__DBL_MAX__;
    int maxI = 0;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (int i = 0; i < int(alpha_vecs.size()); ++i)
    {
        vector<double> alpha_temp = alpha_vecs[i].GetValues();
        double v_temp = 0;
        for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
        {
            v_temp += alpha_temp[it->first] * (it->second);
        }

        if (v_temp > v_max)
        {
            v_max = v_temp;
            maxI = i;
        }
    }

    return maxI;
}

double compute_p_oba(int o, BeliefSparse &b, int a, DecPomdpInterface *decpomdp)
{
    double res = 0;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
    {
        double pr_o_s_newI = 0;
        double pr_sI = it->second;
        int sI = it->first;

        map<int, double> *prob_dist_trans = decpomdp->GetTransProbDist(sI, a);
        map<int, double>::iterator it_trans;

        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            pr_o_s_newI += (it_trans->second) * decpomdp->ObsFunc(o, s_newI, a);
        }
        res += pr_sI * pr_o_s_newI;
    }

    return res;
};

double compute_p_oba(int o, BeliefSparse &b, int a, PomdpInterface *pomdp)
{
    double res = 0;
    // int count = 0;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it;

    for (it = belief_sparse->begin(); it != belief_sparse->end(); it++)
    {
        double pr_o_s_newI = 0;
        double pr_sI = it->second;
        int sI = it->first;

        // sparse representation
        // map<int, double>* prob_dist_trans = pomdp->GetTransProbDist(sI,a);
        // map<int, double>::iterator it_trans;

        unordered_map<int, double> *prob_dist_trans = pomdp->GetTransProbDist(sI, a);
        unordered_map<int, double>::iterator it_trans;
        for (it_trans = prob_dist_trans->begin(); it_trans != prob_dist_trans->end(); it_trans++)
        {
            int s_newI = it_trans->first;
            double pr_s_newI = it_trans->second;
            pr_o_s_newI += pr_s_newI * pomdp->ObsFunc(o, s_newI, a);
        }
        res += pr_sI * pr_o_s_newI;
    }
    return res;
};

BeliefSparse Update(PomdpInterface *Pb, BeliefSparse &b, int aI, int oI, double p_oba)
{
    int SizeS = Pb->GetSizeOfS();
    // vector<double> b_aI_value(SizeS);

    map<int, double> pb_states;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it_sI;

    for (it_sI = belief_sparse->begin(); it_sI != belief_sparse->end(); it_sI++)
    {
        int sI = it_sI->first;
        double pr_sI = it_sI->second;
        // sparse representation
        // map<int, double>* prob_dist_trans = Pb->GetTransProbDist(sI,aI);
        // map<int, double>::iterator it;

        unordered_map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, aI);
        unordered_map<int, double>::iterator it;
        for (it = prob_dist_trans->begin(); it != prob_dist_trans->end(); it++)
        {
            int s_newI = it->first;
            double p_s_newI = 0;
            double p_o = Pb->ObsFunc(oI, s_newI, aI);
            if (p_o > 0)
            {
                p_s_newI += it->second * pr_sI;
            }
            double pr_snew_ao = (p_o * p_s_newI) / p_oba;
            pb_states[s_newI] += pr_snew_ao;
        }
    }

    BeliefSparse b_ao(pb_states, SizeS);

    return b_ao;
};

BeliefSparse Update(DecPomdpInterface *Pb, BeliefSparse &b, int aI, int oI, double p_oba)
{
    int SizeS = Pb->GetSizeOfS();

    // cerr << SizeS << endl;
    // vector<double> b_aI_value(SizeS);

    map<int, double> pb_states;

    map<int, double> *belief_sparse = b.GetBeliefSparse();
    map<int, double>::iterator it_sI;

    for (it_sI = belief_sparse->begin(); it_sI != belief_sparse->end(); it_sI++)
    {
        int sI = it_sI->first;
        double pr_sI = it_sI->second;
        // sparse representation
        map<int, double> *prob_dist_trans = Pb->GetTransProbDist(sI, aI);
        map<int, double>::iterator it;
        for (it = prob_dist_trans->begin(); it != prob_dist_trans->end(); it++)
        {
            int s_newI = it->first;
            double p_s_newI = 0;
            double p_o = Pb->ObsFunc(oI, s_newI, aI);
            if (p_o > 0)
            {
                p_s_newI += it->second * pr_sI;
            }
            double pr_snew_ao = (p_o * p_s_newI) / p_oba;
            pb_states[s_newI] += pr_snew_ao;
        }
    }

    BeliefSparse b_ao(pb_states, SizeS);

    return b_ao;
};

BeliefSparse Update(DecPomdpInterface *Pb, BeliefSparse &b, int aI, int oI)
{
    vector<double> b_aI_value(Pb->GetSizeOfS());
    double pr_oba = compute_p_oba(oI, b, aI, Pb);
    if (pr_oba > 0)
    {
        return Update(Pb, b, aI, oI, pr_oba);
    }
    else
    {
        cout << "Error! p_oba is 0!" << endl;
        throw "Error! p_oba is 0!";
    }
};

BeliefSparse Update(PomdpInterface *Pb, BeliefSparse &b, int aI, int oI)
{
    vector<double> b_aI_value(Pb->GetSizeOfS());
    double pr_oba = compute_p_oba(oI, b, aI, Pb);
    if (pr_oba > 0)
    {
        return Update(Pb, b, aI, oI, pr_oba);
    }
    else
    {
        cout << "Error! p_oba is 0!" << endl;
        throw "Error! p_oba is 0!";
    }
};

// From MADP codes, ImportValueFunction
vector<AlphaVector> ImportValueFunction(const string &filename)
{
    vector<AlphaVector> V;

    int lineState = 0; /* lineState=0 -> read action
                      * lineState=1 -> read values
                      * lineState=2 -> empty line, skip */
    int nrStates = -1;
    bool first = true;
    int action = 0;
    double value;
    int n;
    vector<double> values;

    ifstream fp(filename.c_str());
    if (!fp)
    {
        cerr << "AlphaVectorPlanning::ImportValueFunction: failed to "
             << "open file " << filename << endl;
    }

    string buffer;
    while (!getline(fp, buffer).eof())
    {
        switch (lineState)
        {
        case 0:
            // read action
            //            action=strtol(buffer,NULL,10);
            {
                istringstream is(buffer);
                while (is >> n)
                {
                    if (n >= 0)
                    {
                        action = n;
                    }
                }
            }
            lineState++;
            break;
        case 1:
            // read values
            values.clear();

            {
                istringstream is(buffer);
                while (is >> value)
                    values.push_back(value);
            }

            if (first)
            {
                nrStates = values.size();
                first = false;
            }

            // create new alpha vector and store it
            {
                AlphaVector alpha(nrStates);
                alpha.SetAction(action);
                alpha.SetValues(values);
                V.push_back(alpha);
            }

            lineState++;
            break;
        case 2:
            // do nothing, line is empty
            lineState = 0;
            break;
        }
    }

    return (V);
}
