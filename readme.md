# InfJESP
Infinite-horizon Joint Equilibrium-based Search for Policies

This is a C++ implementation of the InfJESP algorithm [1]. It aims to solve Dec-POMDP problems at infinite horizons by finding Nash-equilibrium solutions.

[1] [**Solving infinite-horizon Dec-POMDPs using Finite State Controllers within JESP**]() *Yang You, Vincent Thomas, Francis Colas and Olivier Buffet.* In _Proceedings of The 33rd IEEE International Conference on Tools with Artificial Intelligence_ (ICTAI2021), November 1-3, 2021.

## Cloning the project

To clone the project (with its two submodules):

git clone --recurse-submodules https://gitlab.inria.fr/anr-fcw/infjesp.git

## Installation

Tested systems:

* Linux
* Mac OS X

Dependencies:

* cmake >= 2.8
* C++ 11 is required
* APPL Offline (which implements SARSOP, a POMDP solver):
  https://bigbird.comp.nus.edu.sg/pmwiki/farm/appl/
  (git submodule in
  third_party_dependencies/sarsop/ )
* tinyxml (a small library for reading xml files):
  https://sourceforge.net/projects/tinyxml/
  (git submodule in
  third_party_dependencies/tinyxml/


First you need to compile the SARSOP codes for the 3rd-party POMDP solver, then back to the main folder path:

```
cd third_party_dependencies/sarsop/src/
make
cd ../../../
```

Once you have all required dependencies, you can simply execute the following commands from the project's main folder:

```
cmake ./
make
```

## Quick Start

The executable file is will be generated called "InfJESP", to run the program, just type:

1. Solving a Dec-POMDP problem with randomly initialized FSCs
    ```
    ./InfJESP ./problems/dectiger.dpomdp
    ```
2. Solving a Dec-POMDP problem with heuristic initializations
    * Initialization with extracted stochastic FSCs from the MPOMDP result
        ```
        ./InfJESP ./problems/dectiger.dpomdp -s
        ```
    * Initialization with extracted deterministic FSCs from the MPOMDP result
        ```
        ./InfJESP ./problems/dectiger.dpomdp -d
        ```

The log files are located in the "./logs" folder which contains all the information at each iteration.

A user guide is provided by typing:

```
./InfJESP -h
```

or

```
./InfJESP --help
```

A cleaning shell is written for cleaning the CMake compiling generated files, just run：

```
sh cleanCMAKE.sh
```


## Acknowledgements

Some 3rd parties codes are used in this project. SARSOP (https://github.com/AdaCompNUS/sarsop) is used as the POMDP solver. TinyXML (http://www.grinninglizard.com/tinyxml/) is used to parse POMDP and Dec-POMDP files.

We also use the same file formats for the POMDP (.pomdp), Dec-POMDP (.dpomdp) and alpha-vectors as in the MADP project (http://www.fransoliehoek.net/fb/index.php?fuseaction=software.madp).

