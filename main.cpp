/* This file has been written and/or modified by the following people:
 *
 * You Yang;
 * Vincent Thomas;
 * Francis Colas;
 * Olivier Buffet.
 * 
 * This program is an implementation of algorithm inf-JESP. */

#include <iostream>
#include "Include/Interfaces/DecPomdpInterface.h"
#include "Include/Parser/ParserPOMDP.h"
#include "Include/Parser/ParserDecPOMDP.h"
#include "Include/Parser/ParserDecPOMDPSparse.h"
#include <stdlib.h>
#include <stdio.h>
#include "Include/Planner/InfJesp.h"
#include "time.h"
#include <getopt.h>
extern int optind, opterr, optopt;
extern char *optargi;
static struct option long_options[] =
    {
        {"help", no_argument, NULL, 'h'},
        {"initRand", required_argument, NULL, 'r'},
        {"initMS", no_argument, NULL, 's'},
        {"initMD", no_argument, NULL, 'd'},
        {"initGivenFSC", required_argument, NULL, 'f'},
        {"formalization_init_node", no_argument, NULL, 'i'},
        {"precision", required_argument, NULL, 'p'},
        {"number_restarts", required_argument, NULL, 'n'},
        {"best_response", required_argument, NULL, 'b'},
        {"output", required_argument, NULL, 'o'},

};

using namespace std;

// Indicate the pomdp solver path and set a fixed timeout of 5s
// const string pomdp_solver_path = "/Users/yyou/Desktop/inria/appl/src/pomdpsol --timeout 5";
const string pomdp_solver_path = "/home/yang/Desktop/INRIA/sarsop/src/pomdpsol --timeout 5";
void UsageManual();

double DecPomdpPoliciesEvaluation(DecPomdpInterface *Pb, vector<FSCBase> &FSCs);
void BuildMixNashEqilibria(DecPomdpInterface *Pb, vector<vector<FSCBase>> &Set_All_FSCs);

int main(int argc, char *argv[])
{
    string DecPomdpfilename;
    string outfile;
    string out_name;
    string GivenFSCPath;
    double epsilon = 0.001; //defualt value, will be used for SARSOP and FSC evaluation
    int restart = 1;
    int index = 0;
    int MAX_FSC_size = 5;
    int init_type = 0;
    int index_sampled_agent_for_best_response = -1;
    int formalization_best_response_pomdp_type = 0; //default momdp formalization
    int c = 0;
    while (EOF != (c = getopt_long(argc, argv, "hsdir:p:n:b:o:f:", long_options, &index)))
    {
        switch (c)
        {
        case 'h':
            // printf("we get option -h，index %d\n",index);
            UsageManual();
            return 0;
        case 's':
            init_type = 1;
            break;
        case 'i':
            formalization_best_response_pomdp_type = 1;
            break;
        case 'd':
            init_type = 2;
            break;
        case 'f':
            init_type = 3;
            GivenFSCPath = optarg;
            break;
        case 'n':
            restart = stoi(optarg);
            break;
        case 'b':
            index_sampled_agent_for_best_response = stoi(optarg);
            break;
        case 'r':
            MAX_FSC_size = stoi(optarg);
            break;
        case 'p':
            epsilon = stod(optarg);
            break;
        case 'o':
            out_name = optarg;
            break;
        case '?':
            printf("unknow option:%c\n", optopt);
            break;
        default:
            break;
        }
    }

    if (argc < 2)
    {
        cout << "Argument Error!" << endl;
        cout << "Use -h or --help to see the user guide" << endl;
        return -1;
    }
    else
    {
        // cout << argc << endl;
        DecPomdpfilename = argv[argc - 1];
        cout << argv[argc - 1] << endl;
    }

    if (restart <= 0)
    {
        cout << "Restart number should bigger than 0!" << endl;
        return -1;
    }

    // DecPomdpInterface *Pb = new ParsedDecPOMDP(DecPomdpfilename);
    DecPomdpInterface *Pb = new ParsedDecPOMDPSparse(DecPomdpfilename);

    // CheckDecPOMDP(Pb);

    cout << "Formalization type:" << formalization_best_response_pomdp_type << endl;
    outfile += "./logs/" + out_name + to_string(Pb->GetDiscount()) + "_" + to_string(epsilon) + "_" + to_string(restart) + ".csv";
    ofstream outlogs;
    outlogs.open(outfile.c_str());
    outlogs << "Restart"
            << ","
            << "Iteration"
            << ","
            << "AgentI"
            << ","
            << "FscNodes"
            << ","
            << "StateSizeBeforeElimination"
            << ","
            << "StateSizeAfterElimination"
            << ","
            << "ValueFSC"
            << ","
            << "ValueAlphaVecs"
            << ","
            << "IterTime"
            << ","
            << "V_maxFSC, TotalTime, Agent0FscNodeSize, Agent1FscNodeSize, Iterations" << endl;

    cout << "epsilon:" << epsilon << endl;
    cout << "restarts:" << restart << endl;
    cout << "init_type:" << init_type << endl;

    double V_max = -DBL_MAX;

    // set<vector<FSCBase>> Set_All_FSCs;
    vector<vector<FSCBase>> Set_All_FSCs;


    for (int i = 0; i < restart; i++)
    {
        double V_temp = 0;
        if (init_type == 0)
        {
            InfJesp *InfiniteJespPlanner = new InfJesp(Pb, MAX_FSC_size, epsilon, formalization_best_response_pomdp_type);
            V_temp = InfiniteJespPlanner->Plan(i, outlogs);
            vector<FSCBase> FSCs_temp = InfiniteJespPlanner->GetFSCs();
            // Set_All_FSCs.insert(FSCs_temp);
            Set_All_FSCs.push_back(FSCs_temp);

            delete InfiniteJespPlanner;
        }
        else if (init_type == 3) // init with a given FSC (For agent 0)
        {
            vector<FSCBase> InitFSCs(2);
            FSCBase fsc_init_0(GivenFSCPath, Pb->GetSizeOfObs(0), formalization_best_response_pomdp_type);
            InitFSCs[0] = fsc_init_0;
            InfJesp *InfiniteJespPlanner = new InfJesp(Pb, InitFSCs, epsilon, formalization_best_response_pomdp_type);
            V_temp = InfiniteJespPlanner->Plan(i, outlogs);
            delete InfiniteJespPlanner;
        }
        else
        {
            if (index_sampled_agent_for_best_response >= 0)
            {
                InfJesp *InfiniteJespPlanner = new InfJesp(Pb, epsilon, init_type, index_sampled_agent_for_best_response, formalization_best_response_pomdp_type);
                V_temp = InfiniteJespPlanner->Plan(i, outlogs);
                delete InfiniteJespPlanner;
            }
            else
            {
                InfJesp *InfiniteJespPlanner = new InfJesp(Pb, epsilon, init_type, formalization_best_response_pomdp_type);
                V_temp = InfiniteJespPlanner->Plan(i, outlogs);
                delete InfiniteJespPlanner;
            }
        }

        // Init InfJesp with given FSCs
        // InfJesp* InfiniteJespPlanner = new InfJesp(Pb, FSCs, epsilon,pomdp_solver_path);

        // Save V_max FSCs
        if (V_temp > V_max)
        {
            V_max = V_temp;
            string command_rm_old_max = "rm -rf ./TempFiles/ValueMaxFSCs/*";
            string command_cp = "cp -r ./TempFiles/TempFSCs/. ./TempFiles/ValueMaxFSCs";
            string command_rm_temp = "rm -rf ./TempFiles/TempFSCs/*";
            const char *p_rm_old_max = command_rm_old_max.data();
            const char *p_cp = command_cp.data();
            const char *p_rm_temp = command_rm_temp.data();
            system(p_rm_old_max);
            system(p_cp);
            system(p_rm_temp);
        }
    }
    cout << "---- All " << restart << " restarts finished ----" << endl;
    cout << "The V_max found in restarts:" << V_max << endl;

    outlogs.close();

    delete Pb;


    return 0;
}

void UsageManual()
{
    printf("Inf-JESP Solver:\n\n");
    printf(" InfJesp DecPomdpFilePath \n");
    printf(" or\n");
    printf(" InfJesp DecPomdpFilePath [--initialization method] [--precison for planner and policy evaluation] [--restarts number]\n");
    printf(" or\n");
    printf(" InfJesp --help\n\n");
    printf("=================================\n");
    printf("COMMAND-LINE OPTIONS\n");
    printf("=================================\n\n");
    printf("Miscellaneous options:\n\n");
    printf("    -h or --help                        Print user guide.\n\n");
    printf("Inf-JESP solver options:\n\n");
    printf("    -s or --initMS                      Initialization with extracted stochastic FSCs from the MPOMDP result. No additional args needed.\n");
    printf("    -d or --initMD                      Initialization with extracted deterministic FSCs from the MPOMDP result. No additional args needed.\n");
    printf("    -r or --initRand                    Initialization with random FSCs. Need to give the maxium FSC size. This is the default initialization method.\n\n");
    // printf("    --timeout                    Set a timelimit for Inf-JESP in seconds.\n\n");
    printf("Pomdp Planner and evaluation options:\n\n");
    printf("    -p or --precision                   Set a precision for both pomdp planner and policy evaluation parts. Default is 0.01.\n\n");
    // printf("    --plannerlimit               Set a time limit for the POMDP planner. Default is 5s.\n\n");
    printf("Restarts options:\n\n");
    printf("    -n or --number_restarts             Set a restarts number. Default is 1 (No restarts).\n\n");
    printf("Best Response Mode options:\n\n");
    printf("    -b or --best_response               Give a sampled agent index for building the best response for other agents. Must be used with initMS or initMD.\n\n");
    printf("Output options:\n\n");
    printf("    -o or --output                      Give a head name of the output file.\n\n");
};
